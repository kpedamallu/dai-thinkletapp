package com.daikin.thinklet.utils

import android.content.Context
import android.widget.Toast

// This class will handle the logging mechanism, can process based on our
// requirement like saving into file sending to server etc
object LogUtils {

    internal fun print(message: String) {
        println(message)
    }

    internal fun Log(tag: String, message: String) {
        android.util.Log.i(tag, message)
    }

    fun Logw(tag: String, message: String, tr: Throwable) {
        android.util.Log.w(tag, message, tr)
    }

    internal fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}