package com.daikin.thinklet.utils

import android.R
import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit

object AppUtils {
    const val TAG = "AppUtils"
    private var mProgressBar: ProgressBar? = null
    private var mContext: Context? = null

    fun getDate(
        date: String,
        requiredTimeOnly: Boolean = false, additionTime: Int = -1
    ): String {
        // ignoring millis even if it is coming from server
        val persistFileFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")//.SSS

        var requiredUIFormat: SimpleDateFormat? = null
        requiredUIFormat = if (requiredTimeOnly) {
            SimpleDateFormat("HH:mm:ss")
        } else {
            SimpleDateFormat("yyyy-MM-dd")
        }
        var convertedDateString: String = ""
        var date: Date = persistFileFormat.parse(date.trim())
        if (additionTime != -1) {
            date = Date(date.time + additionTime * 60 * 1000)
        }
        convertedDateString = requiredUIFormat.format(date)
        println(convertedDateString)
        return convertedDateString
    }
    fun getEndDateWithDurationDate(
        date: String, additionTime: Int = -1
    ): String {
        // ignoring millis even if it is coming from server
        val persistFileFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")//.SSS
        var requiredUIFormat: SimpleDateFormat? = null
        requiredUIFormat = SimpleDateFormat("yyyy-MM-dd")
        var convertedDateString: String = ""
        var date: Date = persistFileFormat.parse(date.trim())
        if (additionTime != -1) {
            date = Date(date.time + additionTime * 60 * 1000)
        }
        convertedDateString = requiredUIFormat.format(date)
        println(convertedDateString)
        return convertedDateString
    }

    fun convertDateWithDay(date: String): String {
        // ignoring millis even if it is coming from server
        val persistFileFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var requiredUIFormat: SimpleDateFormat = SimpleDateFormat("EEEE, MMM dd, yy")
        var convertedDateString: String = ""
        var date: Date = persistFileFormat.parse(date.trim())
        convertedDateString = requiredUIFormat.format(date)
        println(convertedDateString)
        return convertedDateString
    }

    fun convertDate(
        date: String,
        requiredTimeOnly: Boolean = false,
        additionTime: Int = -1
    ): String {
        // ignoring millis even if it is coming from server
        val persistFileFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")//.SSS

        var requiredUIFormat: SimpleDateFormat? = null
        requiredUIFormat = if (requiredTimeOnly) {
            SimpleDateFormat("HH:mm aa")
        } else {
            SimpleDateFormat("MMM dd")
        }
        var convertedDateString: String = ""
        var date: Date = persistFileFormat.parse(date.trim())
        if (additionTime != -1) {
            date = Date(date.time + additionTime * 60 * 1000)
        }
        convertedDateString = requiredUIFormat.format(date)
        println(convertedDateString)
        return convertedDateString
    }

    fun createMeetingJsonObject(
        isTeams: Boolean, title: String?,
        desc: String?, startTime: String, duration: Int,
        joinURL: String?, deviceIDs: ArrayList<String>,
        zoomMeetingID: Long = -1, zoomPassword: String? = null
    ): JsonObject {
        var meetingJsonObject = JsonObject()
        meetingJsonObject.addProperty("title", title)
        meetingJsonObject.addProperty("description", desc)
        meetingJsonObject.addProperty("startTime", startTime)
        meetingJsonObject.addProperty("duration", duration)
        var meetingChannelObject = JsonObject()
        if (isTeams) {
            // If Teams
            meetingChannelObject.addProperty("joinUrl", joinURL)
            meetingJsonObject.add("teamsByUrl", meetingChannelObject)
        } else {
            // If Zoom
            meetingChannelObject.addProperty("zoomMeetingId", zoomMeetingID)
            meetingChannelObject.addProperty("password", zoomPassword)
            meetingChannelObject.addProperty("joinUrl", joinURL)
            meetingJsonObject.add("zoomByIdExt", meetingChannelObject)
        }
        var devicesArray = JsonArray()
        for (device in deviceIDs) {
            devicesArray.add(device)
        }
        meetingJsonObject.add("devices", devicesArray)
        println("LinkletRemoteRepository.createMeetingJsonObject - $meetingJsonObject")
        return meetingJsonObject
    }


    fun getStartTimeForCreateMeeting(
        meetingStartDate: String?,
        meetingStartTime: String?
    ): String {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")
        val startDatePart: LocalDate = LocalDate.parse(meetingStartDate)
        val startTimePart: LocalTime = LocalTime.parse(meetingStartTime)
        val startDT: LocalDateTime = LocalDateTime.of(startDatePart, startTimePart)
        val startTime = formatter.format(startDT)
        Log.d(TAG, "start time for create meeting :$startTime")
        return startTime
    }

    fun getDurationForCreateMeeting(startDateTime: String, endDateTime: String): Long {
        val obj = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        val start: Date = obj.parse(startDateTime.trim())
        val end: Date = obj.parse(endDateTime.trim())
        val timeDifference = end.time - start.time
        val minutesDifference: Long = TimeUnit.MILLISECONDS.toMinutes(timeDifference)
        Log.d(TAG, "duration for create meeting :$minutesDifference")
        return minutesDifference
    }


    fun showProgress(activity: Activity) {
        try {
            dismissProgress(activity)
            activity?.getWindow()?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            );
            val layout = activity.findViewById<View>(R.id.content).rootView as ViewGroup
            mProgressBar = ProgressBar(activity, null, R.attr.progressBarStyleLargeInverse)
            mProgressBar!!.setIndeterminate(true)
            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
            )
            val rl = RelativeLayout(activity)
            rl.gravity = Gravity.CENTER
            rl.addView(mProgressBar)
            layout.addView(rl, params)
            mProgressBar?.visibility = View.VISIBLE
        } catch (ex: java.lang.Exception) {
            ex.toString()
        }
    }

    fun dismissProgress(activity: Activity) {
        try {
            activity?.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            mProgressBar?.visibility = View.GONE
        } catch (ex: java.lang.Exception) {
            ex.toString()
        }
    }


}