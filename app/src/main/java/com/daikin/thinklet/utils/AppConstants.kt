package com.daikin.thinklet.utils

object AppConstants {
    //db constants
    const val DB_VERSION=4
    const val DB_NAME="linklet.db"
    const val ZOOM_MEETING_PWD="123456"
    const val INNOMINDS_URL="innominds.com"

    const val ARG_MEETING_ITEM = "meeting_item"

    const val CALL_TYPE_TEAMS = "Teams"
    const val CALL_TYPE_ZOOM = "Zoom"

    const val EVENT_TYPE_CREATE = 1
    const val EVENT_TYPE_EDIT = 2

}