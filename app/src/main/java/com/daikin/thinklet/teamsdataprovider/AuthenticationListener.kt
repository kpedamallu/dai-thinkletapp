package com.daikin.thinklet.teamsdataprovider

interface AuthenticationListener {
    fun onUserAuthenticationSucess(graphAPIToken:String, communicationAPItoken:String,requestType : Int)
    fun onAuthenticationFailed(error: String,requestType: Int)
}

interface GraphAPIResultListener {
    fun onSuccess(result:Any)
    fun onFailed(error: String)
}