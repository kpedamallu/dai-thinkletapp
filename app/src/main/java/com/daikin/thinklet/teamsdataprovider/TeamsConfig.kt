package com.daikin.thinklet.teamsdataprovider

object TeamsConfig {
    const val END_POINT = "endpoint=https://thinkletcommunicationresource.communication.azure.com/;accesskey=yhrNBLJfXAriBOVrAWUzkRXL5CTasZGd527/7sW3oc3K0IWiMreIPjGG40Ss+ohfV2hQTaZUkXdkKCdS2O5fXg=="
    const val CLIENT_ID = "00255878-7034-4966-b615-218f6503529f"
    val scopes: Array<String> = arrayOf(
        "User.Read",
        "User.ReadWrite",
        "OnlineMeetingArtifact.Read.All",
        "OnlineMeetings.ReadWrite",
        "Calendars.ReadWrite",
        "User.Read.All"

    )

     val communicationServiceScopes: Array<String> = arrayOf(
        "https://auth.msft.communication.azure.com/Teams.ManageCalls"

    )

    const val CREATE_MEETING = 1
    const val JOIN_MEETING = 2
    const val USER_PROFILE = 3

}