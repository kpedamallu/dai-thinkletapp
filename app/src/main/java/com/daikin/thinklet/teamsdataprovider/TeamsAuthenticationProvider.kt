package com.daikin.thinklet.teamsdataprovider

import android.app.Activity
import android.text.TextUtils
import android.util.Log
import com.daikin.thinklet.R
import com.microsoft.identity.client.*
import com.microsoft.identity.client.exception.MsalException

@Suppress("DEPRECATION")
object TeamsAuthenticationProvider {
    private val TAG = "TeamsAuthenticationProvider"
    private var mSingleAccountApp: ISingleAccountPublicClientApplication? = null
    private var mAccount: IAccount? = null
    private lateinit var graphAPIAccessToken: String
    private lateinit var communicationAPIAccessToken: String
    private lateinit var authenticationListener: AuthenticationListener
    private var requestType: Int = 0


    fun getToken(activity: Activity, listener: AuthenticationListener, requestType: Int) {
        authenticationListener = listener
        this.requestType = requestType

        if ((this::graphAPIAccessToken.isInitialized && !TextUtils.isEmpty(graphAPIAccessToken)) &&
            (this::communicationAPIAccessToken.isInitialized && !TextUtils.isEmpty(
                communicationAPIAccessToken
            ))
        ) {
            authenticationListener.onUserAuthenticationSucess(
                graphAPIAccessToken, communicationAPIAccessToken, requestType
            )
            return
        }

        // Creates a PublicClientApplication object with res/raw/auth_config_single_account.json
        PublicClientApplication.createSingleAccountPublicClientApplication(activity,
            R.raw.auth_config_single_account,
            object : IPublicClientApplication.ISingleAccountApplicationCreatedListener {
                override fun onCreated(application: ISingleAccountPublicClientApplication) {
                    /**
                     * This test app assumes that the app is only going to support one account.
                     * This requires "account_mode" : "SINGLE" in the config json file.
                     */

                    mSingleAccountApp = application
                    Log.d(TAG, "Going to load the account  $mSingleAccountApp")
                    loadAccount(activity)
                }

                override fun onError(exception: MsalException) {
                    Log.d(
                        TAG, "exception createSingleAccountPublicClientApplication ::::::$exception"
                    )
                    listener?.onAuthenticationFailed(exception.localizedMessage, requestType)
                }
            })
    }

    /**
     * Load the currently signed-in account, if there's any.
     */

    private fun loadAccount(activity: Activity) {
        if (mSingleAccountApp == null) {
            return
        }
        mSingleAccountApp?.getCurrentAccountAsync(object :
            ISingleAccountPublicClientApplication.CurrentAccountCallback {
            override fun onAccountLoaded(activeAccount: IAccount?) {
                mAccount = activeAccount

                if (mAccount == null) {
                    Log.d(TAG, "Active account is null going to login in the microsoft account")

                    mSingleAccountApp!!.signIn(
                        activity,
                        null,
                        TeamsConfig.communicationServiceScopes,
                        getCommunicationServiceAuthInteractiveCallback()!!
                    )

                } else {
                    Log.d(TAG, "Active account Loaded and going to acquire token for Resources")
                    mSingleAccountApp!!.acquireTokenSilentAsync(
                        TeamsConfig.communicationServiceScopes,
                        mAccount!!.authority,
                        getAuthSilentCallbackForCommunicationToken()!!
                    )
                }
            }

            override fun onAccountChanged(priorAccount: IAccount?, currentAccount: IAccount?) {
                if (currentAccount == null) {
                    Log.d(TAG, "account changed")
                }
                authenticationListener.onAuthenticationFailed("account changed", requestType)
            }

            override fun onError(exception: MsalException) {
                exception.printStackTrace()
                authenticationListener.onAuthenticationFailed(
                    "" + exception.localizedMessage,
                    requestType
                )
            }
        })
    }

    /**
     * Callback used for interactive request.
     * If succeeds we use the access token to call the Microsoft Graph.
     * Does not check cache.
     */

    private fun getCommunicationServiceAuthInteractiveCallback(): AuthenticationCallback? {
        return object : AuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult) {

                /* Successfully got a communication token, use it to call a protected resource - communication service */
                mAccount = authenticationResult.account
                Log.d(TAG, "Active account loaded & Successfully authenticated")
                communicationAPIAccessToken = authenticationResult.accessToken
                Log.d(TAG, "communication service token $communicationAPIAccessToken")

                mSingleAccountApp!!.acquireTokenSilentAsync(
                    TeamsConfig.scopes, mAccount!!.authority, getAuthSilentCallbackForGraphToken()!!
                )

            }

            override fun onError(exception: MsalException) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: $exception")
                logout()
            }

            override fun onCancel() {

                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.")
            }
        }
    }

    /**
     * Callback used in for silent acquireToken calls.
     */

    private fun getAuthSilentCallbackForGraphToken(): SilentAuthenticationCallback? {
        return object : SilentAuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult) {
                Log.d(
                    TAG,
                    "Successfully authenticated and graph token is  " + authenticationResult.accessToken
                )
                graphAPIAccessToken = authenticationResult.accessToken
                authenticationListener.onUserAuthenticationSucess(
                    graphAPIAccessToken, communicationAPIAccessToken, requestType
                )
            }

            override fun onError(exception: MsalException) {

                /* Failed to acquireToken */
                Log.d(
                    TAG, "Authentication failed getAuthSilentCallbackForGraphToken: $exception"
                )
                logout()
//                authenticationListener.onAuthenticationFailed(
//                    "" + exception.localizedMessage,
//                    requestType
//                )

            }
        }
    }

    private fun getAuthSilentCallbackForCommunicationToken(): SilentAuthenticationCallback? {
        return object : SilentAuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult) {
                Log.d(
                    TAG,
                    "Successfully authenticated and communication token is  " + authenticationResult.accessToken
                )
                communicationAPIAccessToken = authenticationResult.accessToken
                mSingleAccountApp!!.acquireTokenSilentAsync(
                    TeamsConfig.scopes, mAccount!!.authority, getAuthSilentCallbackForGraphToken()!!
                )

            }

            override fun onError(exception: MsalException) {
                /* Failed to acquireToken */
                Log.d(
                    TAG,
                    "Authentication failed getAuthSilentCallbackForCommunicationToken: $exception"
                )
                logout()
//                authenticationListener.onAuthenticationFailed(
//                    "" + exception.localizedMessage,
//                    requestType
//                )
            }
        }
    }


    private fun logout() {

        /**
         * Removes the signed-in account and cached tokens from this app (or device, if the device is in shared mode).
         */
        graphAPIAccessToken = ""
        communicationAPIAccessToken = ""
        mSingleAccountApp?.signOut(object : ISingleAccountPublicClientApplication.SignOutCallback {
            override fun onSignOut() {
                mAccount = null
                Log.d(TAG, "logged out")
                authenticationListener.onAuthenticationFailed("logged out", requestType)
            }

            override fun onError(exception: MsalException) {
                Log.d(TAG, "log out error: $exception")
                authenticationListener.onAuthenticationFailed(
                    "" + exception.localizedMessage,
                    requestType
                )
            }
        })
    }

}