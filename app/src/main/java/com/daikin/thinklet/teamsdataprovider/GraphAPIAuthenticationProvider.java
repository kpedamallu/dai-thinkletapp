package com.daikin.thinklet.teamsdataprovider;

import com.microsoft.graph.authentication.IAuthenticationProvider;

import java.net.URL;
import java.util.concurrent.CompletableFuture;

public class GraphAPIAuthenticationProvider implements IAuthenticationProvider {

   private CompletableFuture<String> accessTokenFuture;

   public GraphAPIAuthenticationProvider(String accessToken) {
      this.accessTokenFuture = new CompletableFuture<>();
      this.accessTokenFuture.complete(accessToken);
   }

   @Override
   public CompletableFuture<String> getAuthorizationTokenAsync(URL requestUrl) {
      return this.accessTokenFuture;
   }

}