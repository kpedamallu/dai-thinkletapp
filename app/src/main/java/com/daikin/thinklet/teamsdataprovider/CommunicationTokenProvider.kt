package com.daikin.thinklet.teamsdataprovider

import android.util.Log
import com.azure.communication.identity.CommunicationIdentityClientBuilder
import com.azure.communication.identity.models.GetTokenForTeamsUserOptions


class CommunicationTokenProvider {
    fun fetchUserToken(accessToken: String, userObjectID: String): String {
        var token = ""
        try {
            val connectionString = TeamsConfig.END_POINT
            val communicationIdentityClient = CommunicationIdentityClientBuilder()
                .connectionString(connectionString)
                .buildClient()
            val options = GetTokenForTeamsUserOptions(
                accessToken,
                TeamsConfig.CLIENT_ID,
                userObjectID
            )
            val accessToken = communicationIdentityClient.getTokenForTeamsUser(options);
            System.out.println("Token: " + accessToken.token);
            token = accessToken.token
        } catch (ex: Exception) {
            ex.toString()
        }
        Log.d("CommunicationTokenProvider", "token user $token")
        return token

    }


}

