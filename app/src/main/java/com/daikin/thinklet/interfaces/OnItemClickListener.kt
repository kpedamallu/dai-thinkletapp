package com.daikin.thinklet.interfaces

interface OnItemClickListener {
    fun onItemClick(position: Int)
}
