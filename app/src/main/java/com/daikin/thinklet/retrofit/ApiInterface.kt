package com.daikin.thinklet.retrofit

import com.daikin.thinklet.model.ItemsMeeting
import com.daikin.thinklet.model.LinkletDevices
import com.daikin.thinklet.model.MeetingList
import com.daikin.thinklet.model.UserProfile
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @GET("me")
    fun getLinkletUserProfile(): Call<UserProfile>

    @GET("devices")
    fun getDevicesList(@Query("group") groupId :String?): Call<LinkletDevices>

    @GET("meetings?limit=10&offset=0")
    fun getListOfMeetings(@Query("group") groupId :String?): Call<MeetingList>

    @POST("meetings")
    fun createMeeting(@Body meetingObject: JsonObject, @Query("group") groupId :String?): Call<ItemsMeeting>

    @DELETE("meetings/{meetingId}")
    fun deleteMeeting(@Path("meetingId") meetingID:String, @Query("remote") isDeleteFromRemote :Boolean, @Query("group") groupId :String?): Call<ResponseBody> //,@Query("group") group :String

    @PUT("meetings/{meetingId}")
    fun updateMeeting(@Path("meetingId") meetingID:String,@Body meetingObject: JsonObject, @Query("group") groupId :String?): Call<ItemsMeeting>
}