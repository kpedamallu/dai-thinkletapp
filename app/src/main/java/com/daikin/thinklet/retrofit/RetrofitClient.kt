package com.daikin.thinklet.retrofit

import android.content.Context
import com.daikin.thinklet.ThinkletApplication
import com.daikin.thinklet.model.PreferencesManager
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

object RetrofitClient {

    const val MainServer = "https://apis.linklet.ai/api/v1/"

    val retrofitClient: Retrofit.Builder by lazy {
        val logging = CustomInterceptor(ThinkletApplication.context)

        val okhttpClient = OkHttpClient.Builder()
        okhttpClient.addInterceptor(logging)

        Retrofit.Builder()
            .baseUrl(MainServer)
            .client(okhttpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
    }

    val apiInterface: ApiInterface by lazy {
        retrofitClient
            .build()
            .create(ApiInterface::class.java)
    }

    private class CustomInterceptor(context: Context?) : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val sessionManager = PreferencesManager.get<String>(PreferencesManager.KEY_USER_AUTH_TOKEN)
            val requestBuilder = chain.request().newBuilder()
            sessionManager?.let {
                requestBuilder.addHeader("Authorization", "Bearer $it")
                println("CustomInterceptor.intercept - $it")
            } ?: run {
                println("CustomInterceptor.intercept - auth token not available in shared prefs")
            }
            requestBuilder.addHeader("x-api-key", "IQzsKlDU3JPAyiYiQXOeDAxkS2QsHyfs")

            return chain.proceed(requestBuilder.build())

        }
    }
}
