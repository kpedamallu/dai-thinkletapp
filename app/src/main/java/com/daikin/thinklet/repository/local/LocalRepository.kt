package com.daikin.thinklet.repository.local

import android.content.Context
import androidx.lifecycle.LiveData
import com.daikin.thinklet.database.AppDatabase
import com.daikin.thinklet.database.dao.UserDao
import com.daikin.thinklet.database.dbmodels.TBUSer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalRepository(context: Context) {
    // TOOD create DAO methods to save/ retrieve data from database
    private lateinit var userDao: UserDao

    init {
        val db = AppDatabase.getInstance(context)
        db?.let {
            userDao = it.userDao()
        }
    }

    fun getUser(id: Long): LiveData<TBUSer> {
        return userDao.getUser(id)
    }

    suspend fun saveUser(user: TBUSer) {
        withContext((Dispatchers.IO)) {
            userDao.insertUser(user)

        }
    }

}