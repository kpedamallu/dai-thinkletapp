package com.daikin.thinklet.repository.remote

import androidx.lifecycle.MutableLiveData
import com.daikin.thinklet.ThinkletApplication
import com.daikin.thinklet.model.*
import com.daikin.thinklet.retrofit.RetrofitClient.apiInterface
import com.daikin.thinklet.utils.Event
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LinkletRemoteRepository {

    fun userProfileAPI() {
        val call = apiInterface.getLinkletUserProfile()

        call.enqueue(object : Callback<UserProfile?> {
            override fun onResponse(
                call: Call<UserProfile?>,
                response: Response<UserProfile?>
            ) {
                if (response.isSuccessful) {
                    println("LinkletRemoteRepository.userLoginAPI.onResponse-- " + response.body()?.name)
                    profileResponseLiveData?.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<UserProfile?>, t: Throwable) {
                println("LinkletRemoteRepository.userLoginAPI.0n failure data")
                profileResponseLiveData?.postValue(null)
            }
        })
    }

    fun getDevicesListAPI() {
        val call = apiInterface.getDevicesList(PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE)?.defaultGroup?.id)

        call.enqueue(object : Callback<LinkletDevices?> {
            override fun onResponse(
                call: Call<LinkletDevices?>,
                response: Response<LinkletDevices?>
            ) {
                if (response.isSuccessful) {
                    println("LinkletRemoteRepository.getDevicesListAPI.onResponse-- " + response.body())
                    listDevicesResponseLiveData?.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<LinkletDevices?>, t: Throwable) {
                println("LinkletRemoteRepository.getDevicesListAPI.0n failure data")
                listDevicesResponseLiveData?.postValue(null)
            }
        })
    }

    fun getMeetingListAPI() {
        val call = apiInterface.getListOfMeetings(PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE)?.defaultGroup?.id)

        call.enqueue(object : Callback<MeetingList?> {
            override fun onResponse(
                call: Call<MeetingList?>,
                response: Response<MeetingList?>
            ) {
                if (response.isSuccessful) {
                    println("LinkletRemoteRepository.getMeetingListAPI.onResponse-- " + response.body())
                    listOfMeetingResponseLiveData?.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<MeetingList?>, t: Throwable) {
                println("LinkletRemoteRepository.getMeetingListAPI.0n failure data")
                listOfMeetingResponseLiveData?.postValue(null)
            }
        })
    }

    fun createMeetingAPI(meetingObject: JsonObject) {

        val call = apiInterface.createMeeting(meetingObject, PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE)?.defaultGroup?.id)

        call.enqueue(object : Callback<ItemsMeeting?> {
            override fun onResponse(
                call: Call<ItemsMeeting?>,
                response: Response<ItemsMeeting?>
            ) {
                if (response.isSuccessful) {
                    println("LinkletRemoteRepository.createMeetingAPI.onResponse-- " + response.body())
                    createdMeetingLiveData?.postValue(Event(response.body()))
                }
            }

            override fun onFailure(call: Call<ItemsMeeting?>, t: Throwable) {
                println("LinkletRemoteRepository.createMeetingAPI.0n failure data")
                createdMeetingLiveData?.postValue(Event(null))
            }
        })
    }

    fun deleteMeetingAPI(meetingId: String, deleteFromRemote: Boolean, group: String) {

        val call = apiInterface.deleteMeeting(meetingId, deleteFromRemote, PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE)?.defaultGroup?.id)

        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                if (response.isSuccessful) {
                    println("LinkletRemoteRepository.deleteMeetingLiveData.onResponse-- " + response.body())
                    deleteMeetingLiveData?.postValue(Event("Success"))
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                println("LinkletRemoteRepository.deleteMeetingLiveData.0n failure data")
                deleteMeetingLiveData?.postValue(Event(t.message))
            }
        })
    }

    fun updateMeetingAPI(meetingId: String, meetingObject: JsonObject) {
        val call = apiInterface.updateMeeting(meetingId, meetingObject, PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE)?.defaultGroup?.id)

        call.enqueue(object : Callback<ItemsMeeting?> {
            override fun onResponse(
                call: Call<ItemsMeeting?>,
                response: Response<ItemsMeeting?>
            ) {
                if (response.isSuccessful) {
                    println("LinkletRemoteRepository.updateMeetingapi.onResponse-- " + response.body())
                    updateMeetingLiveData?.postValue(Event(response.body()))
                }
            }

            override fun onFailure(call: Call<ItemsMeeting?>, t: Throwable) {
                println("LinkletRemoteRepository.updateMeetingapi.0n failure data")
                updateMeetingLiveData?.postValue(Event(null))
            }
        })
    }

    companion object {
        var profileResponseLiveData: MutableLiveData<UserProfile?>? = MutableLiveData()
        var listDevicesResponseLiveData: MutableLiveData<LinkletDevices?>? = MutableLiveData()
        var listOfMeetingResponseLiveData: MutableLiveData<MeetingList?>? = MutableLiveData()
        var createdMeetingLiveData: MutableLiveData<Event<ItemsMeeting?>>? = MutableLiveData()
        var deleteMeetingLiveData: MutableLiveData<Event<String?>>? = MutableLiveData()
        var updateMeetingLiveData: MutableLiveData<Event<ItemsMeeting?>>? = MutableLiveData()
    }
}