package com.daikin.thinklet.repository.external

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.azure.android.communication.common.CommunicationTokenCredential
import com.azure.android.communication.common.CommunicationTokenRefreshOptions
import com.azure.android.communication.ui.calling.CallComposite
import com.azure.android.communication.ui.calling.CallCompositeBuilder
import com.azure.android.communication.ui.calling.models.CallCompositeRemoteOptions
import com.azure.android.communication.ui.calling.models.CallCompositeTeamsMeetingLinkLocator
import com.daikin.thinklet.model.MeetingRequestData
import com.daikin.thinklet.teamsdataprovider.CommunicationTokenProvider
import com.daikin.thinklet.teamsdataprovider.GraphAPIAuthenticationProvider
import com.daikin.thinklet.teamsdataprovider.GraphAPIResultListener
import com.microsoft.graph.logger.DefaultLogger
import com.microsoft.graph.logger.LoggerLevel
import com.microsoft.graph.models.*
import com.microsoft.graph.requests.GraphServiceClient
import kotlinx.coroutines.*
import okhttp3.Request
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

object TeamsRepository {
    private val TAG = "TeamsRepository"
    private lateinit var graphClient: GraphServiceClient<Request>
    private val DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

    private fun getGraphClient(accessToken: String): GraphServiceClient<Request> {
        if (!this::graphClient.isInitialized) {

            // Create default logger to only log errors
            val logger = DefaultLogger()
            logger.setLoggingLevel(LoggerLevel.DEBUG)

            var authProvider = GraphAPIAuthenticationProvider(accessToken)
            graphClient = GraphServiceClient
                .builder()
                .logger(logger)
                .authenticationProvider(authProvider)
                .buildClient()


        }
        return graphClient
    }

    fun createMeeting(
        accessToken: String,
        meetingRequestData: MeetingRequestData
    ): String? {
        try {
            val tz: TimeZone = TimeZone.getDefault()
            System.out.println(
                "TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT)
                    .toString() + " Timezone id :: " + tz.getID()
            )

            // time inputs parsing
            val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)

            // start time
            val startDatePart: LocalDate = LocalDate.parse(meetingRequestData.startDate)
            val startTimePart: LocalTime = LocalTime.parse(meetingRequestData.StartTime)
            val startDT: LocalDateTime = LocalDateTime.of(startDatePart, startTimePart)
            val startTime = formatter.format(startDT)
            Log.d(TAG, startTime)

            // end time
            val endDatePart: LocalDate =
                LocalDate.parse(meetingRequestData.endDate)
            val endTimePart: LocalTime =
                LocalTime.parse(meetingRequestData.endtime)
            val endDT: LocalDateTime = LocalDateTime.of(endDatePart, endTimePart)
            val endTime = formatter.format(endDT)
            Log.d(TAG, endTime)

            val event = Event()
            event.subject = meetingRequestData.meetingSubject
            val body = ItemBody()
            body.contentType = BodyType.HTML
            body.content = meetingRequestData.meetingDescription
            event.body = body
            val start = DateTimeTimeZone()
            start.dateTime = startTime  //"2023-02-22T18:00:00"
            start.timeZone = tz.id //"India Standard Time" //"Pacific Standard Time"
            event.start = start
            val end = DateTimeTimeZone()
            end.dateTime = endTime
            end.timeZone = tz.id
            event.end = end

            val attendeesList = LinkedList<Attendee>()
            for (user in meetingRequestData.attendencies) {
                val attendees = Attendee()
                val emailAddress = EmailAddress()
                emailAddress.address = user.trim()
                attendees.emailAddress = emailAddress
                attendees.type = AttendeeType.REQUIRED
                attendeesList.add(attendees)
                Log.d(TAG, "user added $user")
            }
            event.attendees = attendeesList
            event.allowNewTimeProposals = true
            event.isOnlineMeeting = true
            event.onlineMeetingProvider = OnlineMeetingProviderType.TEAMS_FOR_BUSINESS

            val meeting = getGraphClient(accessToken).me().events().buildRequest().post(event)

            Log.d(TAG, "meeting scheduled " + meeting?.onlineMeeting?.joinUrl)
            return meeting?.onlineMeeting?.joinUrl
        } catch (ex: java.lang.Exception) {
            Log.d(TAG, "meeting scheduled exception$ex")
            return null
        }

    }

    fun getUserProfile(accessToken: String): User? {
        try {
            val user = getGraphClient(accessToken).me()
                .buildRequest()
                .get()
            Log.d(TAG, "user profile " + user?.birthday)
            Log.d(TAG, "user profile " + user?.givenName)
            Log.d(TAG, "user profile " + user?.id)
            return user
        } catch (ex: Exception) {
            Log.d(TAG, "user profile error" + ex.localizedMessage)
            return null
        }
    }

    /*fun getUserProfile(accessToken: String, graphAPIResultListener: GraphAPIResultListener) {
        try {
            CoroutineScope(GlobalScope.coroutineContext).launch {

                getGraphClient(accessToken)?.let {
                    val user = it.me()
                        .buildRequest()
                        .get()
                    launch(Dispatchers.Main) {
                        Log.d(TAG, "user profile " + user?.birthday)
                        Log.d(TAG, "user profile " + user?.givenName)
                        Log.d(TAG, "user profile " + user?.id)
//                        graphAPIResultListener?.onSucess(user.id)
                    }

                }
            }

        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
            Log.d(TAG, "user profile exception" + ex.localizedMessage)
            graphAPIResultListener?.onFailed(ex.localizedMessage)
        }
    }*/


    fun startCallComposite(
        token: String,
        meetingLink: String,
        context: Context,
        userDisplayName: String,
        userObjectID: String
    ) {
        val communicationTokenRefreshOptions =
            CommunicationTokenRefreshOptions({ fetchToken(token, userObjectID) }, true)
        val communicationTokenCredential =
            CommunicationTokenCredential(communicationTokenRefreshOptions)
        val locator = CallCompositeTeamsMeetingLinkLocator(meetingLink)
        val remoteOptions = CallCompositeRemoteOptions(
            locator,
            communicationTokenCredential,
            userDisplayName,
        )
        val callComposite: CallComposite = CallCompositeBuilder().build()
        callComposite.launch(context, remoteOptions)

        callComposite.addOnErrorEventHandler { callCompositeErrorEvent ->
            println(callCompositeErrorEvent.errorCode)
        }


    }

    private fun fetchToken(token: String, userObjectID: String): String? {
//        return CommunicationTokenProvider().fetchUserToken(token,userObjectID)
        return runBlocking {
            withContext(Dispatchers.IO) {
                CommunicationTokenProvider().fetchUserToken(token, userObjectID)
            }
        }
    }


}