package com.daikin.thinklet.repository.external

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Base64
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.daikin.thinklet.ThinkletApplication
import com.daikin.thinklet.model.CustomZoomObj
import com.daikin.thinklet.model.MeetingRequestData
import com.daikin.thinklet.model.Zoom
import com.daikin.thinklet.model.ZoomAccessToken
import com.daikin.thinklet.teamsdataprovider.TeamsConfig
import com.daikin.thinklet.utils.AppConstants
import com.daikin.thinklet.utils.Event
import com.daikin.zoomsample.MeetingInvitees
import com.daikin.zoomsample.ScheduleMeeting
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.HttpUrl
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import us.zoom.sdk.*
import java.security.MessageDigest
import java.security.SecureRandom

object ZoomRepository {
    var CLIENT_ID = "vlV6t2OlS5mVu67BOyouag"
    var CLIENT_SECRET = "L6LdQnWtiE94spiO1v3STvt9D1cGQ3QN"
    val OAUTH_URL =
        "https://zoom.us/oauth/authorize?response_type=code&client_id=vlV6t2OlS5mVu67BOyouag&redirect_uri=https%3A%2F%2Finnominds.com"
    var accessToken = ""
    var zak: String = ""

    var zoomMeetingURLLiveData: MutableLiveData<Event<CustomZoomObj?>>? = MutableLiveData()

    private fun buildUrl(code: String) = HttpUrl.Builder()
        .scheme("https")
        .host("zoom.us")
        .addPathSegment("oauth")
        .addPathSegment("token")
        .addQueryParameter("grant_type", "authorization_code")
        .addQueryParameter("code", code)
        .addQueryParameter("redirect_uri", "https://innominds.com")
        .addQueryParameter("code_verifier", CodeChallengeHelper.verifier)
        .build()

    private fun buildZakUrl() = HttpUrl.Builder()
        .scheme("https")
        .host("api.zoom.us")
        .addPathSegment("v2")
        .addPathSegment("users")
        .addPathSegment("me")
        .addPathSegment("token")
        .addQueryParameter("type", "zak")
        .build()

    private fun buildMeetingUrl() = HttpUrl.Builder()
        .scheme("https")
        .host("api.zoom.us")
        .addPathSegment("v2")
        .addPathSegment("users")
        .addPathSegment("me")
        .addPathSegment("meetings")
        .build()

    fun initializeZoomSDK(context: Context?, requestType: Int) {
         initializeZoomSDKForCreateMeeting(context)
        if (requestType == TeamsConfig.CREATE_MEETING) {
            CodeChallengeHelper.createCodeVerifier()
            val uri = Uri.parse(OAUTH_URL)
                .buildUpon()
                .appendQueryParameter(
                    "code_challenge",
                    CodeChallengeHelper.getCodeChallenge(CodeChallengeHelper.verifier!!)
                )
                .appendQueryParameter("code_challenge_method", "S256")
                .build()

            println("code_challenge " + CodeChallengeHelper.verifier)
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.setDataAndType(uri, "text/html")
            browserIntent.addCategory(Intent.CATEGORY_BROWSABLE)
            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            ThinkletApplication.context?.startActivity(browserIntent)
        }
    }

    private fun initializeZoomSDKForCreateMeeting(context: Context?) {
        var sdk = ZoomSDK.getInstance()
        var sdkParams = ZoomSDKInitParams()
        sdkParams.appKey = CLIENT_ID
        sdkParams.appSecret = CLIENT_SECRET
        sdkParams.domain = "zoom.us"
        sdkParams.enableLog = false

        val initializeListener: ZoomSDKInitializeListener = object : ZoomSDKInitializeListener {
            override fun onZoomSDKInitializeResult(i: Int, i1: Int) {
                println("Initialized successfully")
            }

            override fun onZoomAuthIdentityExpired() {}
        }
        sdk.initialize(context, initializeListener, sdkParams)
    }

    fun initializeZoomSDKForJoinMeeting(requestType: Int, zoomObj: Zoom?, context: Context) {
        var sdk = ZoomSDK.getInstance()
        var sdkParams = ZoomSDKInitParams()
        sdkParams.appKey = CLIENT_ID
        sdkParams.appSecret = CLIENT_SECRET
        sdkParams.domain = "zoom.us"
        sdkParams.enableLog = false

        val initializeListener: ZoomSDKInitializeListener = object : ZoomSDKInitializeListener {
            override fun onZoomSDKInitializeResult(i: Int, i1: Int) {
                println("Initialized successfully")
                if (requestType == TeamsConfig.JOIN_MEETING) {
                    startMeetingWithSDK(
                        zoomObj?.zoomMeetingId.toString(), "Guest", zoomObj?.password.toString(),
                        context
                    )
                }
            }

            override fun onZoomAuthIdentityExpired() {}
        }
        sdk.initialize(context, initializeListener, sdkParams)
    }

    private fun startMeetingWithSDK(
        meetingID: String,
        name: String,
        pwd: String,
        context: Context
    ) {
        var meetingService = ZoomSDK.getInstance().meetingService
        var joinMeetingOptions = JoinMeetingOptions()
        val joinMeetingParams = JoinMeetingParams()
        joinMeetingParams.meetingNo = meetingID
        joinMeetingParams.displayName = name
        joinMeetingParams.password = pwd
        meetingService.joinMeetingWithParams(context, joinMeetingParams, joinMeetingOptions)
    }

    class CodeChallengeHelper {
        companion object {
            var verifier: String? = null
            fun createCodeVerifier() {
                val secureRandom = SecureRandom()
                val code = ByteArray(32)
                secureRandom.nextBytes(code)
                verifier = Base64.encodeToString(
                    code,
                    Base64.URL_SAFE or Base64.NO_WRAP or Base64.NO_PADDING
                )
            }

            fun getCodeChallenge(verifier: String): String {
                val bytes: ByteArray = verifier.toByteArray(Charsets.US_ASCII)
                val md: MessageDigest = MessageDigest.getInstance("SHA-256")
                md.update(bytes, 0, bytes.size)
                val digest: ByteArray = md.digest()
                return Base64.encodeToString(
                    digest,
                    Base64.URL_SAFE or Base64.NO_WRAP or Base64.NO_PADDING
                )
            }
        }
    }


    fun createMeeting(client: OkHttpClient, meetingRequestData: MeetingRequestData,selectDeviceItem: String?) {
        var mainJSONObject: JSONObject = JSONObject()
        mainJSONObject.put("topic", meetingRequestData.meetingSubject)
        mainJSONObject.put("type", "2")
        mainJSONObject.put("start_time", meetingRequestData.startDateTime)
        mainJSONObject.put("duration", meetingRequestData.timeDuration)
        mainJSONObject.put("default_password", false)
        mainJSONObject.put("password", AppConstants.ZOOM_MEETING_PWD)

        var settingsJson: JSONObject = JSONObject()
        settingsJson.put("host_video", true)
        settingsJson.put("participant_video", true)
        settingsJson.put("join_before_host", true)
        settingsJson.put("mute_upon_entry", true)
        settingsJson.put("watermark", true)
        settingsJson.put("audio", "both")
        settingsJson.put("auto_recording", "cloud")

      //  settingsJson.put("meeting_invitees", convertMeetingInvitesList( meetingRequestData.attendencies))
        mainJSONObject.put("settings", settingsJson)

        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            mainJSONObject.toString()
        )
        val request = Request.Builder()
            .addHeader("authorization", "Bearer $accessToken")
            .addHeader("Content-Type", "application/json")
            .url(buildMeetingUrl())
            .post(body)
            .build()
        val response = client.newCall(request).execute()
        Log.d("Meeting schdeule--", "Response${response.code}")
        val gson = Gson()
        var mMeetingsc = gson.fromJson(response.body?.string(), ScheduleMeeting::class.java)
        println("Meeting URL ---" + mMeetingsc.startUrl)
        var zoomObj = Zoom()
        zoomObj.zoomMeetingId = mMeetingsc.id
        zoomObj.joinUrl = mMeetingsc.startUrl
        zoomObj.password = mMeetingsc.password
        var customZoomObj = CustomZoomObj(zoomObj,meetingRequestData,selectDeviceItem)
        zoomMeetingURLLiveData?.postValue(Event(customZoomObj))
    }

    private fun startMeeting(zak: String) {
        val meetingService =
            ZoomSDK.getInstance().meetingService

        val startParams = StartMeetingParamsWithoutLogin().apply {
            zoomAccessToken = zak
            meetingNo = "74876287260"
        }
        println("MainActivity.startMeeting $startParams")

        val result = meetingService.startMeetingWithParams(
            ThinkletApplication.context,
            startParams,
            StartMeetingOptions()
        )
        if (result == MeetingError.MEETING_ERROR_SUCCESS) {
            // The SDK will attempt to join the meeting.
        }
    }


    fun parseCode(dataUri: String, meetingRequestData: MeetingRequestData,selectDeviceItem:String?): String? {
        val uri = Uri.parse(dataUri)
        Log.d("Token -- Zoom ---", uri.toString())
        return uri?.getQueryParameter("code")?.also {
            getAccessToken(it, meetingRequestData,selectDeviceItem)
        }
    }

    private fun getAccessToken(code: String, meetingRequestData: MeetingRequestData,selectDeviceItem: String?) {
        val client = OkHttpClient()
        CoroutineScope(Dispatchers.IO).launch {
            runCatching {

                requestAccessToken(code, client)
                getZak(client)
                createMeeting(client, meetingRequestData,selectDeviceItem)
            }
            launch(Dispatchers.Main) {

            }
        }
    }

    private fun requestAccessToken(code: String, client: OkHttpClient) {
        val encoded = Base64.encodeToString(
            "$CLIENT_ID:$CLIENT_SECRET".toByteArray(),
            Base64.URL_SAFE or Base64.NO_WRAP
        )
        val request = Request.Builder()
            .addHeader("Authorization", "Basic $encoded")
            .addHeader("Content-Type", "application/x-www-form-urlencoded")
            .url(buildUrl(code))
            .post("{}".toRequestBody())
            .build()
        val response = client.newCall(request).execute()
        println("verifier " + CodeChallengeHelper.verifier)
        Log.d("Access token--", "AccessToken${response.code}")
        val gson = Gson()
        var mUser = gson.fromJson(response.body?.string(), ZoomAccessToken::class.java)
        println(mUser.access_token)
        accessToken = mUser.access_token
        Log.d("AT--", "AccessToken$accessToken")
    }


    private fun getZak(client: OkHttpClient) {
        val url = buildZakUrl()
        println("getZak $accessToken")
        val request = Request.Builder()
            .addHeader("authorization", "Bearer $accessToken")
            .url(url)
            .get()
            .build()
        val response = client.newCall(request).execute()
        zak = response.body?.string()!!
        Log.d("ZOOM--", "ZAK$zak")
    }

    private fun convertMeetingInvitesList(attendees:List<String>):ArrayList<MeetingInvitees> {

        var attendeesList:ArrayList<MeetingInvitees> = ArrayList(attendees.size)

        for (obj in attendees) {
            var meetingInvitee= MeetingInvitees()
            meetingInvitee.email = obj
            attendeesList.add(meetingInvitee)
        }
        return  attendeesList

    }
}