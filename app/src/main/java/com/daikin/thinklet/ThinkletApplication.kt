package com.daikin.thinklet

import android.app.Application
import android.content.Context
import com.daikin.thinklet.model.PreferencesManager
import com.daikin.thinklet.utils.LogUtils

// TODO App level initialization will be done here
class ThinkletApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        PreferencesManager.with(this)
    }

    companion object {
        var context: Context? = null
    }
    override fun onLowMemory() {
        super.onLowMemory()
        LogUtils.Log("ThinkletApplication", "on low memory")
    }

}
