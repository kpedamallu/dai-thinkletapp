package  com.daikin.zoomsample

import com.google.gson.annotations.SerializedName


data class Settings(

    @SerializedName("host_video") var hostVideo: Boolean? = null,
    @SerializedName("participant_video") var participantVideo: Boolean? = null,
    @SerializedName("cn_meeting") var cnMeeting: Boolean? = null,
    @SerializedName("in_meeting") var inMeeting: Boolean? = null,
    @SerializedName("join_before_host") var joinBeforeHost: Boolean? = null,
    @SerializedName("jbh_time") var jbhTime: Int? = null,
    @SerializedName("mute_upon_entry") var muteUponEntry: Boolean? = null,
    @SerializedName("watermark") var watermark: Boolean? = null,
    @SerializedName("use_pmi") var usePmi: Boolean? = null,
    @SerializedName("approval_type") var approvalType: Int? = null,
    @SerializedName("audio") var audio: String? = null,
    @SerializedName("auto_recording") var autoRecording: String? = null,
    @SerializedName("enforce_login") var enforceLogin: Boolean? = null,
    @SerializedName("enforce_login_domains") var enforceLoginDomains: String? = null,
    @SerializedName("alternative_hosts") var alternativeHosts: String? = null,
    @SerializedName("alternative_host_update_polls") var alternativeHostUpdatePolls: Boolean? = null,
    @SerializedName("close_registration") var closeRegistration: Boolean? = null,
    @SerializedName("show_share_button") var showShareButton: Boolean? = null,
    @SerializedName("allow_multiple_devices") var allowMultipleDevices: Boolean? = null,
    @SerializedName("registrants_confirmation_email") var registrantsConfirmationEmail: Boolean? = null,
    @SerializedName("waiting_room") var waitingRoom: Boolean? = null,
    @SerializedName("request_permission_to_unmute_participants") var requestPermissionToUnmuteParticipants: Boolean? = null,
    @SerializedName("registrants_email_notification") var registrantsEmailNotification: Boolean? = null,
    @SerializedName("meeting_authentication") var meetingAuthentication: Boolean? = null,
    @SerializedName("encryption_type") var encryptionType: String? = null,
    @SerializedName("approved_or_denied_countries_or_regions") var approvedOrDeniedCountriesOrRegions: ApprovedOrDeniedCountriesOrRegions? = ApprovedOrDeniedCountriesOrRegions(),
    @SerializedName("breakout_room") var breakoutRoom: BreakoutRoom? = BreakoutRoom(),
    @SerializedName("alternative_hosts_email_notification") var alternativeHostsEmailNotification: Boolean? = null,
    @SerializedName("device_testing") var deviceTesting: Boolean? = null,
    @SerializedName("focus_mode") var focusMode: Boolean? = null,
    @SerializedName("enable_dedicated_group_chat") var enableDedicatedGroupChat: Boolean? = null,
    @SerializedName("private_meeting") var privateMeeting: Boolean? = null,
    @SerializedName("email_notification") var emailNotification: Boolean? = null,
    @SerializedName("host_save_video_order") var hostSaveVideoOrder: Boolean? = null,
    @SerializedName("meeting_invitees" ) var meetingInvitees : ArrayList<MeetingInvitees> = arrayListOf()

)

data class MeetingInvitees (

  @SerializedName("email" ) var email : String? = null

)