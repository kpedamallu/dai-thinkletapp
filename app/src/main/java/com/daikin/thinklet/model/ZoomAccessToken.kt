package com.daikin.thinklet.model

import kotlinx.serialization.Serializable

@Serializable
data class ZoomAccessToken(
    val access_token: String,
    val token_type: String,
    val refresh_token: String,
    val expires_in: Long,
    val scope: String
)
