package  com.daikin.zoomsample

import com.google.gson.annotations.SerializedName


data class BreakoutRoom (

  @SerializedName("enable" ) var enable : Boolean? = null

)