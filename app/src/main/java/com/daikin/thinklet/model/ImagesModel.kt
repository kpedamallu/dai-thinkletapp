package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ImagesModel(
    @SerializedName("images")
    val images: ArrayList<Int>? = ArrayList(),
) : Serializable