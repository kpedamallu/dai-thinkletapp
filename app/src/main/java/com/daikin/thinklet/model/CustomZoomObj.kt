package com.daikin.thinklet.model

data class CustomZoomObj(val zoom: Zoom, val meeting: MeetingRequestData?,val selectedDevice:String?) {
    var zoomObj:Zoom? = zoom
    var meetingRequestData:MeetingRequestData? = meeting
    var selectedDeviceID :String?=selectedDevice
}