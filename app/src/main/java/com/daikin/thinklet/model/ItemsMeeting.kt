package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class ItemsMeeting(
    @SerializedName("id") var id: String? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("description") var description: String? = null,
    @SerializedName("startTime") var startTime: String? = null,
    @SerializedName("duration") var duration: Int? = null,
    @SerializedName("zoom") var zoom: Zoom? = Zoom(),
    @SerializedName("teams") var teams: Teams? = Teams(),
    @SerializedName("numberOfPhotos") var numberOfPhotos: Int? = null,
    @SerializedName("devices") var devices: ArrayList<Devices> = arrayListOf(),
    @SerializedName("createdBy") var createdBy: CreatedBy? = CreatedBy(),
    @SerializedName("createdAt") var createdAt: String? = null,
    @SerializedName("updatedAt") var updatedAt: String? = null
):java.io.Serializable