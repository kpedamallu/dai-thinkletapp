package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class MeetingList(
    @SerializedName("items") var items: ArrayList<ItemsMeeting> = arrayListOf(),
    @SerializedName("limit") var limit: Int? = null,
    @SerializedName("offset") var offset: Int? = null,
    @SerializedName("total") var total: Int? = null
)