package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class Teams(
    @SerializedName("joinUrl") var joinUrl: String? = null
):java.io.Serializable