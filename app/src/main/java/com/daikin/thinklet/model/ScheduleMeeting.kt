package  com.daikin.zoomsample

import com.google.gson.annotations.SerializedName


data class ScheduleMeeting(

    @SerializedName("uuid") var uuid: String? = null,
    @SerializedName("id") var id: Long? = null,
    @SerializedName("host_id") var hostId: String? = null,
    @SerializedName("host_email") var hostEmail: String? = null,
    @SerializedName("topic") var topic: String? = null,
    @SerializedName("type") var type: Int? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("start_time") var startTime: String? = null,
    @SerializedName("duration") var duration: Int? = null,
    @SerializedName("timezone") var timezone: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("start_url") var startUrl: String? = null,
    @SerializedName("join_url") var joinUrl: String? = null,
    @SerializedName("password") var password: String? = null,
    @SerializedName("h323_password") var h323Password: String? = null,
    @SerializedName("pstn_password") var pstnPassword: String? = null,
    @SerializedName("encrypted_password") var encryptedPassword: String? = null,
    @SerializedName("settings") var settings: Settings? = Settings(),
    @SerializedName("pre_schedule") var preSchedule: Boolean? = null
)