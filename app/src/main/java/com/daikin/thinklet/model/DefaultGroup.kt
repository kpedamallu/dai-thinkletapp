package com.daikin.thinklet

import com.google.gson.annotations.SerializedName


data class DefaultGroup(

    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("ownerUserId") var ownerUserId: String? = null,
    @SerializedName("useService") var useService: String? = null,
    @SerializedName("createdAt") var createdAt: String? = null,
    @SerializedName("updatedAt") var updatedAt: String? = null,
    @SerializedName("role") var role: String? = null,
    @SerializedName("permissions") var permissions: ArrayList<String> = arrayListOf()

)