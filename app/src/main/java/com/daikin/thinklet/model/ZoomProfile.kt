package com.daikin.thinklet

import com.google.gson.annotations.SerializedName


data class ZoomProfile(
    @SerializedName("email") var email: String? = null
)