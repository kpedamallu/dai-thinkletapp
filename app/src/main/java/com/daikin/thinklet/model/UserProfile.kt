package com.daikin.thinklet.model

import com.daikin.thinklet.DefaultGroup
import com.daikin.thinklet.ZoomProfile
import com.google.gson.annotations.SerializedName


data class UserProfile(
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("useService") var useService: String? = null,
    @SerializedName("zoom") var zoom: ZoomProfile? = ZoomProfile(),
    @SerializedName("plan") var plan: String? = null,
    @SerializedName("defaultGroup") var defaultGroup: DefaultGroup? = DefaultGroup(),
    @SerializedName("email") var email: String? = null,
    @SerializedName("avatar") var avatar: String? = null
)