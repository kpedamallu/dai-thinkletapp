package com.daikin.thinklet.model


class UserData (
    val userId: Long,
    val userName: String
)