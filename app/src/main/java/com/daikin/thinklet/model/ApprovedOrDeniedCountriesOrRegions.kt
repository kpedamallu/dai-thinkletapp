package  com.daikin.zoomsample

import com.google.gson.annotations.SerializedName


data class ApprovedOrDeniedCountriesOrRegions (

  @SerializedName("enable" ) var enable : Boolean? = null

)