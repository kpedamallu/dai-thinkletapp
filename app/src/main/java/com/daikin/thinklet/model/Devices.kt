package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class Devices(
    @SerializedName("id") var id: String? = null,
    @SerializedName("displayName") var displayName: String? = null
):java.io.Serializable