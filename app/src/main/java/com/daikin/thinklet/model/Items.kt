package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class Items(
    @SerializedName("id") var id: String? = null,
    @SerializedName("imei") var imei: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("status") var status: Status? = Status(),
    @SerializedName("createdAt") var createdAt: String? = null,
    @SerializedName("updatedAt") var updatedAt: String? = null
)