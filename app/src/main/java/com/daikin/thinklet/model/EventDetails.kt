package com.daikin.thinklet.model

data class EventDetails(
    var eventName: String,
    var callType: String?,
    var people: String?,
    var allDay: Boolean?,
    var start: String,
    var end: String,
    var remindTime: String?,
    var location: String,
    var description: String
)