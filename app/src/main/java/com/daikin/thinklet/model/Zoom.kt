package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class Zoom(
    @SerializedName("zoomMeetingId") var zoomMeetingId: Long? = null,
    @SerializedName("joinUrl") var joinUrl: String? = null,
    @SerializedName("password") var password: String? = null,
    @SerializedName("external") var external: Boolean? = null
):java.io.Serializable