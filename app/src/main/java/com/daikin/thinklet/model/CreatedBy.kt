package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class CreatedBy(
    @SerializedName("id") var id: String? = null,
    @SerializedName("name") var name: String? = null
):java.io.Serializable