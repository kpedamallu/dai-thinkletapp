package com.daikin.thinklet.model

data class MeetingRequestData(
    val meetingSubject: String,
    val meetingDescription: String,
    val startDate: String,
    val StartTime: String,
    val endDate: String,
    val endtime: String,
    val attendencies: List<String>,
    val isTeamMeeting: Boolean,
    val startDateTime:String,
    val timeDuration:Long
):java.io.Serializable