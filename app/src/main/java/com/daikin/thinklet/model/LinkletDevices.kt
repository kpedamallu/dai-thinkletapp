package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class LinkletDevices(
    @SerializedName("items") var items: ArrayList<Items> = arrayListOf(),
    @SerializedName("limit") var limit: Int? = null,
    @SerializedName("offset") var offset: Int? = null,
    @SerializedName("total") var total: Int? = null
)