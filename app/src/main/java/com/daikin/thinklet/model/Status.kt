package com.daikin.thinklet.model

import com.google.gson.annotations.SerializedName


data class Status(
    @SerializedName("power") var power: Boolean? = null,
    @SerializedName("battery") var battery: Int? = null,
    @SerializedName("wifiLevel") var wifiLevel: String? = null,
    @SerializedName("wifiState") var wifiState: String? = null,
    @SerializedName("mobileLevel") var mobileLevel: String? = null,
    @SerializedName("mobileState") var mobileState: String? = null,
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("longitude") var longitude: String? = null,
    @SerializedName("inMeeting") var inMeeting: String? = null,
    @SerializedName("hasUpdate") var hasUpdate: String? = null,
    @SerializedName("updateProgress") var updateProgress: String? = null,
    @SerializedName("soundVolume") var soundVolume: String? = null,
    @SerializedName("maxSoundVolume") var maxSoundVolume: String? = null
)