package com.daikin.thinklet.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.daikin.thinklet.R
import com.daikin.thinklet.model.PreferencesManager
import com.daikin.thinklet.model.UserProfile
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository

class ProfileFragment : BaseFragment() {
    private lateinit var editIconLayout:ConstraintLayout
    private lateinit var backIcon:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_profile, container, false)

        editIconLayout = view.findViewById(R.id.profile_edit_bg)
        backIcon = view.findViewById(R.id.back_icon)

        backIcon.setOnClickListener {
            requireActivity().finish()
        }

        editIconLayout.setOnClickListener {
            val fragment = ProfileEditFragment()
            val transaction = parentFragmentManager.beginTransaction()
            transaction.replace(R.id.profile_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        var sendFeedbackLayout:ConstraintLayout = view.findViewById(R.id.send_feedback_layout)
        sendFeedbackLayout.setOnClickListener {
            Toast.makeText(context,"Coming soon", Toast.LENGTH_SHORT).show()
        }

        var privacyPolicyLayout:ConstraintLayout = view.findViewById(R.id.privacy_policy_layout)
        privacyPolicyLayout.setOnClickListener {
            Toast.makeText(context,"Coming soon", Toast.LENGTH_SHORT).show()
        }

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE).let {
            println("ProfileFragment.onViewCreated Observe $it")

            var profileName = requireView().findViewById<View>(R.id.profile_name) as TextView
            profileName.text = it?.name?.split("@")?.get(0) ?: ""

            var emailEditText = requireView().findViewById<View>(R.id.email_edit_text) as EditText
            emailEditText.setText(it?.email)
            emailEditText.isFocusable = false
            emailEditText.isFocusableInTouchMode = false
            emailEditText.isClickable = false
        }
    }
}