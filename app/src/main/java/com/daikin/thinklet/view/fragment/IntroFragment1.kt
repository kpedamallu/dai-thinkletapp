package com.daikin.thinklet.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.daikin.thinklet.R
import com.daikin.thinklet.model.ImagesModel
import com.daikin.thinklet.view.ImageSlideAdapter
import me.relex.circleindicator.CircleIndicator


/**
 * A simple [Fragment] subclass.
 * Use the [IntroFragment1.newInstance] factory method to
 * create an instance of this fragment.
 */
class IntroFragment1 : Fragment() {
    private var imagesModel: ImagesModel? = null
    lateinit var viewPagerAdapter: ImageSlideAdapter
    lateinit var indicator: CircleIndicator
    lateinit var viewPager: ViewPager
    lateinit var intro_heading: TextView
    lateinit var intro_sub: TextView
    lateinit var done_btn: Button
    lateinit var skip_text: TextView
    lateinit var next_btn: ImageView
    var currentPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intro_heading = requireView().findViewById<View>(R.id.intro_heading) as TextView
        intro_sub = requireView().findViewById<View>(R.id.intro_sub) as TextView
        next_btn = requireView().findViewById<View>(R.id.next_btn) as ImageView
        imagesModel = ImagesModel(
            arrayListOf(
                R.drawable.introfragment1_img,
                R.drawable.introfragment2_img,
                R.drawable.introfragment3_img
            )
        )
        viewPager = requireView().findViewById<View>(R.id.viewpager) as ViewPager
        imagesModel?.images?.let {
            viewPagerAdapter = ImageSlideAdapter(requireContext(), it)
            viewPager.adapter = viewPagerAdapter
            indicator = requireView().findViewById(R.id.indicator) as CircleIndicator
            indicator.setViewPager(viewPager)
        }

        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                // Check if this is the page you want.
                println("IntroFragment1.onPageSelected $position")
                currentPosition = position
                when (position) {
                    0 -> {
                        intro_heading.text = getString(R.string.hello_heading1)
                        intro_sub.text = getString(R.string.hello_sub1)
                        done_btn.visibility = View.GONE
                        skip_text.visibility = View.VISIBLE
                        next_btn.visibility = View.VISIBLE
                    }
                    1 -> {
                        intro_heading.text = getString(R.string.hello_heading2)
                        intro_sub.text = getString(R.string.hello_sub2)
                        done_btn.visibility = View.GONE
                        skip_text.visibility = View.VISIBLE
                        next_btn.visibility = View.VISIBLE
                    }
                    2 -> {
                        intro_heading.text = getString(R.string.hello_heading3)
                        intro_sub.text = getString(R.string.hello_sub3)
                        done_btn.visibility = View.VISIBLE
                        skip_text.visibility = View.GONE
                        next_btn.visibility = View.GONE
                    }
                }
            }
        })

        skip_text = requireView().findViewById<View>(R.id.skip_text) as TextView
        skip_text.setOnClickListener {
            findNavController().navigate(R.id.action_intro_to_launch)
//            viewPager.currentItem = currentPosition + 1
//            indicator.animatePageSelected(currentPosition + 1)
        }

        done_btn = requireView().findViewById<View>(R.id.done_btn) as Button
        done_btn.setOnClickListener {
            findNavController().navigate(R.id.action_intro_to_launch)
        }

        next_btn.setOnClickListener {
            when (currentPosition) {
                0 -> {
                    viewPager.setCurrentItem(1, true)
                }
                1 -> {
                    viewPager.setCurrentItem(2, true)

                }
            }
        }
    }
}
