package com.daikin.thinklet.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.daikin.thinklet.R
import com.daikin.thinklet.view.HomeScreenActivity
import com.google.android.material.navigation.NavigationView


class HomeScreenFragment : BaseFragment() {

    private lateinit var showAllText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        replaceFragment(CalenderFragment(),R.id.schedule_content)
        showAllText = view.findViewById(R.id.show_all_text)
        showAllText.setOnClickListener {
            replaceFragment(CalenderFragment(),R.id.fragment_container)
            (activity as HomeScreenActivity)?.bottomNavigationView?.selectedItemId = R.id.schedule
        }
    }
    private fun replaceFragment(fragment: Fragment, scheduleContent: Int) {
        requireActivity().supportFragmentManager.beginTransaction().replace(scheduleContent, fragment).commit();
    }
}