package com.daikin.thinklet.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.daikin.thinklet.R
import com.daikin.thinklet.model.ItemsMeeting
import com.daikin.thinklet.repository.external.ZoomRepository
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository
import com.daikin.thinklet.teamsdataprovider.AuthenticationListener
import com.daikin.thinklet.teamsdataprovider.TeamsAuthenticationProvider
import com.daikin.thinklet.teamsdataprovider.TeamsConfig
import com.daikin.thinklet.utils.AppConstants
import com.daikin.thinklet.utils.AppUtils


private const val ARG_MEETING_DETAILS = "meeting_Details"

class MeetingDetailsFragment : BaseFragment(), OnClickListener, AuthenticationListener {

    private lateinit var nameTextView: TextView
    private lateinit var timeTextView: TextView
    private lateinit var dateTextView: TextView
    private lateinit var descriptionTextView: TextView
    private lateinit var meetingLinkTextView: TextView
    private lateinit var backIcon: ImageView
    private lateinit var joinBtn: Button
    private lateinit var removeCalenderBtn: Button
    private lateinit var meetingItem: ItemsMeeting
    private lateinit var meetingIcon: ImageView
    private lateinit var meetingType: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            meetingItem = it.getSerializable(ARG_MEETING_DETAILS) as ItemsMeeting
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_meeting_details, container, false)
        val editView = view.findViewById<ImageView>(R.id.edit)
        editView.setOnClickListener {
            val dialogFragment =
                EventBottomSheetFragment.newInstance(AppConstants.EVENT_TYPE_EDIT, meetingItem)
            dialogFragment?.show(
                requireActivity().supportFragmentManager,
                EventBottomSheetFragment.TAG
            )
        }
        LinkletRemoteRepository.deleteMeetingLiveData?.removeObservers(viewLifecycleOwner)
        LinkletRemoteRepository.deleteMeetingLiveData?.observe(viewLifecycleOwner) { event ->
            run {
                if (event != null && !event.hasBeenHandled()) {
                    val response = event.contentIfNotHandled
                    println("deleteMeetingLiveData.onViewCreated Observe ${response}")
                    if (response.equals("Success", true)) {
                        Toast.makeText(
                            requireContext(),
                            "Meeting deleted successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                        activity?.finish()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Meeting deletion failed : $response",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nameTextView = view.findViewById(R.id.name)
        timeTextView = view.findViewById(R.id.time)
        dateTextView = view.findViewById(R.id.date)
        backIcon = view.findViewById(R.id.back_icon)
        meetingIcon = view.findViewById(R.id.teams_icon)
        meetingType = view.findViewById(R.id.optionName)
        joinBtn = view.findViewById(R.id.join_btn)
        descriptionTextView = view.findViewById(R.id.description)
        meetingLinkTextView = view.findViewById(R.id.meeting_link_text)
        removeCalenderBtn = view.findViewById(R.id.remove_from_calender)
        backIcon.setOnClickListener(this)
        joinBtn.setOnClickListener(this)
        removeCalenderBtn.setOnClickListener(this)

        LinkletRemoteRepository.updateMeetingLiveData?.observe(requireActivity()) { event ->
            if (event != null) {
                val itemsMeeting = event.getContent()
                itemsMeeting?.let {
                    if(itemsMeeting.id.equals(meetingItem.id,true)) {
                        println("MeetingDetailsFragment.updateMeetingLiveData Observe ${itemsMeeting?.title}")
                        meetingItem = it
                        fillContent(meetingItem)
                    }
                }
            }
        }

        fillContent(meetingItem)

    }

    private fun fillContent(itemMeeting: ItemsMeeting) {
        Log.d("MeetingDetails","fillContent meeting details"+itemMeeting.toString())
        nameTextView.text = itemMeeting?.title
        timeTextView.text = AppUtils.convertDate(itemMeeting?.startTime!!, true) + " - " +
                AppUtils.convertDate(itemMeeting.startTime!!, true, itemMeeting.duration!!)

        dateTextView.text = AppUtils.convertDateWithDay(itemMeeting.startTime!!)
        descriptionTextView.text = itemMeeting?.description
        itemMeeting?.teams?.joinUrl?.let {
            meetingLinkTextView.text = itemMeeting?.teams?.joinUrl
        } ?: kotlin.run {
            meetingLinkTextView.text = itemMeeting?.zoom?.joinUrl
            meetingIcon.setImageResource(R.drawable.zoom)
            meetingType.text = getText(R.string.Zoom)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(details: ItemsMeeting) =
            MeetingDetailsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_MEETING_DETAILS, details)
                }
            }
    }

    override fun onClick(view: View?) {
        val id = view?.id
        if (id == R.id.join_btn) {
            meetingItem?.teams?.joinUrl?.let {
                joinTeamsMeeting()
            } ?: kotlin.run {
                joinZoomMeeting()
            }
        } else if (id == R.id.remove_from_calender) {
            deleteMeeting()
        } else if (id == R.id.back_icon) {
            activity?.finish()
        }
    }

    private fun deleteMeeting() {
        // TODO group as of now we are not handling
        LinkletRemoteRepository().deleteMeetingAPI(meetingItem?.id!!, true, "")
    }


    private fun joinTeamsMeeting() {
        TeamsAuthenticationProvider.getToken(requireActivity(), this, TeamsConfig.JOIN_MEETING)
    }

    private fun joinZoomMeeting() {
        ZoomRepository.initializeZoomSDKForJoinMeeting(
            TeamsConfig.JOIN_MEETING, meetingItem.zoom, requireContext()
        )
    }

    override fun onUserAuthenticationSucess(
        graphAPIToken: String,
        communicationAPItoken: String, requestType: Int
    ) {
        when (requestType) {
            TeamsConfig.JOIN_MEETING ->
                joinMeeting(communicationAPItoken, meetingItem?.teams?.joinUrl!!, graphAPIToken)
        }
    }

    override fun onAuthenticationFailed(error: String, requestType: Int) {
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
    }
}