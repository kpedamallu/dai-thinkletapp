package com.daikin.thinklet.view

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import com.daikin.thinklet.R
import com.daikin.thinklet.model.ItemsMeeting
import com.daikin.thinklet.utils.AppConstants
import com.daikin.thinklet.view.fragment.MeetingDetailsFragment

class MeetingsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meetings)
        val meetingDetails =
            intent.getSerializableExtra(AppConstants.ARG_MEETING_ITEM) as ItemsMeeting
        onBackPressedDispatcher.addCallback(callback)
        replaceFragment(MeetingDetailsFragment.newInstance(meetingDetails))
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.meetings_container, fragment).addToBackStack(null)
            .commit();
    }

    val callback = onBackPressedDispatcher.addCallback(this) {
        finish()
    }

}