package com.daikin.thinklet.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.callback.Callback
import com.auth0.android.provider.WebAuthProvider
import com.daikin.thinklet.R
import com.daikin.thinklet.ThinkletApplication
import com.daikin.thinklet.model.PreferencesManager
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository
import com.daikin.thinklet.view.fragment.CalenderFragment
import com.daikin.thinklet.view.fragment.EventBottomSheetFragment
import com.daikin.thinklet.view.fragment.HomeScreenFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView


class HomeScreenActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var drawer: DrawerLayout
    private lateinit var navigationView: NavigationView
    private lateinit var hamburgerIcon: ImageView
    private lateinit var profileLayout: ConstraintLayout
    lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen_activty)

        bottomNavigationView = findViewById(R.id.bottomNavigationView)
        bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    replaceFragment(HomeScreenFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.schedule -> {
                    replaceFragment(CalenderFragment())
                    return@setOnItemSelectedListener true
                }
                R.id.thinklet -> {
                    Toast.makeText(this,"Thinklet",Toast.LENGTH_SHORT).show()
                    return@setOnItemSelectedListener true
                }
                R.id.chat -> {
                    Toast.makeText(this,"Chat",Toast.LENGTH_SHORT).show()
                    return@setOnItemSelectedListener true
                }
            }
            false
        }
        val toolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        val inflator = LayoutInflater.from(this)
        val v = inflator.inflate(R.layout.toolbar_layout, null)
        v.layoutParams = MarginLayoutParams(
            Toolbar.LayoutParams.MATCH_PARENT,
            Toolbar.LayoutParams.WRAP_CONTENT
        )
        toolbar.addView(v)

        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        toggle.syncState()
        drawer.addDrawerListener(toggle)
        toggle.isDrawerIndicatorEnabled = false
        toggle.toolbarNavigationClickListener = View.OnClickListener {
            println("toggle.toolbarNavigationClickListener")
            if(drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(Gravity.LEFT) //CLOSE Nav Drawer!
            }else{
                drawer.openDrawer(Gravity.LEFT) //OPEN Nav Drawer!
            }
        }


        navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        val headerView = navigationView.getHeaderView(0)
        hamburgerIcon = headerView.findViewById<ImageView>(R.id.hamburger_icon)
        profileLayout = headerView.findViewById(R.id.profile_edit_bg)

        profileLayout.setOnClickListener {
            openProfileActivity()
            drawer.closeDrawer(GravityCompat.START)
        }

        replaceFragment(HomeScreenFragment())

        LinkletRemoteRepository.profileResponseLiveData?.observe(this) { userProfileResponse ->
            println("HomeScreenActivity.onViewCreated Observe ${userProfileResponse?.name}")

            PreferencesManager.put(userProfileResponse, PreferencesManager.KEY_PROFILE)

            println("HomeScreenActivity.onCreate $userProfileResponse")
            (headerView.findViewById<ImageView>(R.id.profile_name) as TextView).text =
                userProfileResponse?.name!!.split("@")[0]
            (headerView.findViewById<ImageView>(R.id.profile_email) as TextView).text =
                userProfileResponse?.email

            val stringBuilder = StringBuilder()
            stringBuilder.append("Hi ")
            stringBuilder.append(userProfileResponse?.name!!.split("@")[0])
            stringBuilder.append(",")
            val finalString = stringBuilder.toString()
            (v.findViewById<ImageView>(R.id.user_name) as TextView).text = finalString
        }

        LinkletRemoteRepository().userProfileAPI()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        var zoomToken:String= ""
        intent?.dataString?.let {
            zoomToken = it
            Log.d("Token -- Zoom",it)
        }
        val fragmentInst: Fragment? = supportFragmentManager.findFragmentByTag(EventBottomSheetFragment.TAG)
        if (fragmentInst is EventBottomSheetFragment)
            (fragmentInst as EventBottomSheetFragment).getZoomToken(zoomToken)
    }

    private fun openProfileActivity() {
        val intent: Intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@HomeScreenActivity)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == R.id.nav_schedules) {
            replaceFragment(CalenderFragment())
            drawer.closeDrawer(GravityCompat.START)
        } else if (id == R.id.nav_logout) {
            showMessageOKCancel("Do you want to logout?") { dialog, which ->
                logout()
            }
        } else if (id == R.id.nav_help) {
            val browserIntent = Intent(Intent.ACTION_VIEW,
                Uri.parse("https://sites.google.com/fairydevices.jp/linklet/"))
            startActivity(browserIntent)
        } else {
            Toast.makeText(applicationContext, "coming soon", Toast.LENGTH_SHORT).show()
        }
        return true;
    }

    private fun replaceFragment(fragment: Fragment) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment).addToBackStack(null)
                .commit();
    }

    private fun logout() {
        var account = Auth0(
            getString(R.string.com_auth0_client_id),
            getString(R.string.com_auth0_domain)
        )
        WebAuthProvider.logout(account)
            .withScheme(getString(R.string.com_auth0_scheme))
            .start(this, object : Callback<Void?, AuthenticationException> {
                override fun onSuccess(payload: Void?) {
                    // The user has been logged out!
                    Toast.makeText(applicationContext, "Logged out Success", Toast.LENGTH_LONG)
                        .show()
                    PreferencesManager.clearAll(application)
                    finish()
                }

                override fun onFailure(exception: AuthenticationException) {
                    Toast.makeText(applicationContext,
                        "Failure: ${exception.getCode()}",
                        Toast.LENGTH_LONG).show()
                }
            })
    }

    val callback = onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            val myFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
            println("HomeScreenActivity.handleOnBackPressed $myFragment")
            if (myFragment != null && myFragment is HomeScreenFragment) {
                showMessageOKCancel("Do you want to logout?") { dialog, which ->
                    logout()
                }
            } else{
                supportFragmentManager.popBackStackImmediate()
            }
        }
    })
}