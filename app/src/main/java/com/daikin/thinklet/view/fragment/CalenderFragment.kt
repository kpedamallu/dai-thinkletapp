package com.daikin.thinklet.view.fragment

import android.content.Context
import android.content.Intent
import android.content.pm.verify.domain.DomainVerificationManager
import android.content.pm.verify.domain.DomainVerificationUserState
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.daikin.thinklet.R
import com.daikin.thinklet.adapter.CalenderAdapter
import com.daikin.thinklet.adapter.OnMeetingClickListener
import com.daikin.thinklet.model.*
import com.daikin.thinklet.repository.external.ZoomRepository
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository
import com.daikin.thinklet.teamsdataprovider.AuthenticationListener
import com.daikin.thinklet.teamsdataprovider.TeamsAuthenticationProvider
import com.daikin.thinklet.teamsdataprovider.TeamsConfig
import com.daikin.thinklet.utils.AppConstants
import com.daikin.thinklet.utils.AppConstants.INNOMINDS_URL
import com.daikin.thinklet.view.MeetingsActivity
import kotlinx.coroutines.*
import java.util.*


class CalenderFragment : BaseFragment(), OnMeetingClickListener, AuthenticationListener {
    private var TAG = "CalenderFragment"
    private lateinit var selectedJoinMeetingURL: String
    private lateinit var calenderRecyclerView: RecyclerView
    private lateinit var calenderAdapter: CalenderAdapter
    private lateinit var addEventButton: ImageView
    private var scheduledMeetingList = ArrayList<ItemsMeeting>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_calender, container, false)
        calenderRecyclerView = view.findViewById(R.id.recyclerview)

        addEventButton = view.findViewById(R.id.fab)

        addEventButton.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && callUserPermissionsActivity()) {
                println("CalenderFragment.onCreateView - Permission not added")
            } else {
                val dialogFragment = EventBottomSheetFragment.newInstance(AppConstants.EVENT_TYPE_CREATE,null)
                dialogFragment?.show(
                    requireActivity().supportFragmentManager,
                    EventBottomSheetFragment.TAG
                )
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calenderAdapter = CalenderAdapter(scheduledMeetingList, context, this)
        calenderRecyclerView.adapter = calenderAdapter
        calenderRecyclerView.layoutManager = LinearLayoutManager(context)

        LinkletRemoteRepository.listOfMeetingResponseLiveData?.observe(viewLifecycleOwner) { meetingList ->
            println("CalenderFragment.onViewCreated Observe ${meetingList?.items}")
            scheduledMeetingList.clear()
            meetingList?.items?.let {
                scheduledMeetingList.addAll(meetingList?.items!!)
                calenderAdapter?.notifyDataSetChanged()
            }

        }

        LinkletRemoteRepository.createdMeetingLiveData?.observe(requireActivity()) { event ->
            if (event != null) {
                val itemsMeeting = event.getContent()
                println("CalenderFragment.createdMeetingLiveData Observe ${itemsMeeting?.title}")
                itemsMeeting?.let {
                    scheduledMeetingList.add(itemsMeeting!!)
                    calenderAdapter?.notifyDataSetChanged()
                }
            }
        }
        LinkletRemoteRepository.updateMeetingLiveData?.observe(requireActivity()) { event ->
            if (event != null) {
                val itemsMeeting = event.getContent()
                println("CalenderFragment.updateMeetingLiveData Observe ${itemsMeeting?.title}")
                itemsMeeting?.let {
                    scheduledMeetingList?.let {
                        val updatedItem =
                            scheduledMeetingList.filter { it.id == itemsMeeting?.id!! }
                        if (updatedItem.isNotEmpty() && scheduledMeetingList.contains(updatedItem.first())) {
                            scheduledMeetingList.remove(updatedItem.first())
                            scheduledMeetingList.add(itemsMeeting)
                            calenderAdapter?.notifyDataSetChanged()
                        }
                    }
                }
            }
        }

        LinkletRemoteRepository.deleteMeetingLiveData?.observe(viewLifecycleOwner) { event ->
            run {
                if (event != null) {
                    println("CalenderFragment.deleteMeetingLiveData Observe}")
                    // refresh ui as deleted some event
                    LinkletRemoteRepository().getMeetingListAPI()
                }
            }

        }

        LinkletRemoteRepository().getMeetingListAPI()

    }

    override fun onItemClick(itemsMeeting: ItemsMeeting) {
        val intent = Intent(activity, MeetingsActivity::class.java)
        intent.putExtra(AppConstants.ARG_MEETING_ITEM, itemsMeeting)
        startActivity(intent)
    }

    override fun onUserAuthenticationSucess(
        graphAPIToken: String,
        communicationAPItoken: String, requestType: Int
    ) {
        when (requestType) {
            TeamsConfig.JOIN_MEETING ->
                joinMeeting(communicationAPItoken, selectedJoinMeetingURL, graphAPIToken)
        }
    }

    override fun onAuthenticationFailed(error: String, requestType: Int) {
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
    }


    override fun onJoinMeetingClick(meetingURL: String) {
        if (TextUtils.isEmpty(meetingURL)) {
            Toast.makeText(requireContext(), "Meeting url is empty", Toast.LENGTH_SHORT).show()
        } else {
            selectedJoinMeetingURL = meetingURL
            TeamsAuthenticationProvider.getToken(requireActivity(), this, TeamsConfig.JOIN_MEETING)
        }
    }

    override fun onZoomMeetingClick(meetingZoomObj: Zoom) {
        println("CalenderFragment.onZoomMeetingClick")
        ZoomRepository.initializeZoomSDKForJoinMeeting(
            TeamsConfig.JOIN_MEETING, meetingZoomObj, requireContext()
        )
    }

    /*
     For android 12 and above os we need to ask the user permission to allow the redirection URL
     */
    @RequiresApi(Build.VERSION_CODES.S)
    fun callUserPermissionsActivity(): Boolean {
        val context: Context = requireActivity()
        val manager = context.getSystemService(
            DomainVerificationManager::class.java
        )
        val userState = manager.getDomainVerificationUserState(context.packageName)

        val hostToStateMap = userState!!.hostToStateMap
        val verifiedDomains: MutableList<String> = ArrayList()
        val selectedDomains: MutableList<String> = ArrayList()
        val unapprovedDomains: MutableList<String> = ArrayList()
        for (key in hostToStateMap.keys) {
            val stateValue = hostToStateMap[key]
            if (stateValue == DomainVerificationUserState.DOMAIN_STATE_VERIFIED) {
                // Domain has passed Android App Links verification.
                verifiedDomains.add(key)
            } else if (stateValue == DomainVerificationUserState.DOMAIN_STATE_SELECTED) {
                // Domain hasn't passed Android App Links verification, but the user has
                // associated it with an app.
                selectedDomains.add(key)
            } else {
                // All other domains.
                unapprovedDomains.add(key)
            }
        }
        // Settings ->  'Open supported links' should be enabled
        // Settings -> In add links, site link should be enabled
        if (!userState.isLinkHandlingAllowed || (selectedDomains.size == 0 || unapprovedDomains.contains(
                INNOMINDS_URL
            ))
        ) {
            val intent = Intent(
                Settings.ACTION_APP_OPEN_BY_DEFAULT_SETTINGS,
                Uri.parse("package:${context.packageName}")
            )
            context.startActivity(intent)
            return true
        }
        return false
    }
}
