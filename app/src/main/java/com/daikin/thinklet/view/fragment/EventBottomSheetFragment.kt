package com.daikin.thinklet.view.fragment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import com.daikin.thinklet.R
import com.daikin.thinklet.model.Items
import com.daikin.thinklet.model.ItemsMeeting
import com.daikin.thinklet.model.MeetingRequestData
import com.daikin.thinklet.repository.external.TeamsRepository
import com.daikin.thinklet.repository.external.ZoomRepository
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository
import com.daikin.thinklet.teamsdataprovider.AuthenticationListener
import com.daikin.thinklet.teamsdataprovider.TeamsAuthenticationProvider
import com.daikin.thinklet.teamsdataprovider.TeamsConfig
import com.daikin.thinklet.utils.AppConstants
import com.daikin.thinklet.utils.AppUtils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

private const val ARG_EVENT_TYPE = "event_type"
private const val ARG_MEETING_DETAILS = "meeting_Details"

class EventBottomSheetFragment : BottomSheetDialogFragment(), AuthenticationListener {

    private lateinit var bottomSheetDialog: View
    private var progressBar: ProgressBar? = null
    private var eventName: EditText? = null
    private var eventTitle: TextView? = null
    private var callTypeSpinner: Spinner? = null
    private var deviceListSpinner: Spinner? = null
    private var peopleEdit: EditText? = null
    private var startDate: Button? = null
    private var startTime: Button? = null
    private var endDate: Button? = null
    private var endTime: Button? = null
    private var description: EditText? = null
    private var eventDone: ImageButton? = null
    private var eventDismiss: ImageButton? = null

    private var callTypeName: String? = null
    private var assignedDeviceList = ArrayList<Items>()
    private var selectedDeviceItem: String? = null
    private var deviceNamesList = ArrayList<String>()
    private var deviceListAdapter: ArrayAdapter<String>? = null
    private lateinit var meetingRequestData: MeetingRequestData
    private var eventType: Int = 0
    private var itemMeeting: ItemsMeeting? = null

    companion object {
        const val TAG = "CreateEvent"

        @JvmStatic
        fun newInstance(eventType: Int, details: ItemsMeeting?) =
            EventBottomSheetFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_EVENT_TYPE, eventType)
                    putSerializable(ARG_MEETING_DETAILS, details)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eventType = it.getInt(ARG_EVENT_TYPE)
            val item = it.getSerializable(ARG_MEETING_DETAILS)
            if (item != null) {
                itemMeeting = item as ItemsMeeting
            }
        }
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {

        // get the views and attach the listener
        bottomSheetDialog = inflater.inflate(
            R.layout.new_event_details_layout, container,
            false
        )
        return bottomSheetDialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        LinkletRemoteRepository().getDevicesListAPI()
        LinkletRemoteRepository.listDevicesResponseLiveData?.observe(viewLifecycleOwner) { deviceListItems ->
            println("Bottomsheet.listDevicesResponseLiveData Observe ${deviceListItems?.items}")
            assignedDeviceList.clear()
            deviceNamesList.clear()
            deviceListItems?.items?.forEach {
                assignedDeviceList.add(it)
                deviceNamesList.add("" + it.name)
            }
            updateCreateMeetingDevices()
        }

        ZoomRepository.zoomMeetingURLLiveData?.observe(requireActivity()) { event ->
            if (event != null && !event.hasBeenHandled()) {
                var zoomMeetingURLReceived = event.contentIfNotHandled
                Log.d("ZOOM", zoomMeetingURLReceived.toString())
                if (this::meetingRequestData.isInitialized) {
                    zoomMeetingURLReceived?.let {
//                        val startTime = AppUtils.getStartTimeForCreateMeeting(
//                            it.meetingRequestData?.startDate,
//                            it.meetingRequestData?.StartTime
//                        )
//                        val duration = AppUtils.getDurationForCreateMeeting(
//                            it.meetingRequestData?.StartTime,
//                            it.meetingRequestData?.endtime
//                        )
                        LinkletRemoteRepository().createMeetingAPI(
                            AppUtils.createMeetingJsonObject(
                                false,
                                it.meetingRequestData?.meetingSubject,
                                it.meetingRequestData?.meetingDescription,
                                it.meetingRequestData?.startDateTime!!,
                                it.meetingRequestData?.timeDuration?.toInt()!!,
                                it.zoomObj?.joinUrl,
                                arrayListOf(it.selectedDevice!!),
                                it.zoomObj?.zoomMeetingId!!, it.zoomObj?.password

                            )
                        )
                    }
                }
            }

        }

        LinkletRemoteRepository.createdMeetingLiveData?.observe(viewLifecycleOwner)
        { event ->
            if (event != null && !event.hasBeenHandled()) {
                val itemsMeeting = event.contentIfNotHandled
                println("Bottomsheet.createdMeetingLiveData Observe ${itemsMeeting?.title}")
                progressBar?.visibility = View.INVISIBLE
                this.dismiss()
            }
        }

        LinkletRemoteRepository.updateMeetingLiveData?.observe(viewLifecycleOwner)
        { event ->
            if (event != null && !event.hasBeenHandled()) {
                val itemsMeeting = event.contentIfNotHandled
                println("Bottomsheet.updateMeetingLiveData Observe ${itemsMeeting?.title}")
                progressBar?.visibility = View.INVISIBLE
                this.dismiss()
            }
        }
    }

    private fun initUI() {
        progressBar = bottomSheetDialog.findViewById<ProgressBar>(R.id.event_progressbar)
        eventTitle = bottomSheetDialog.findViewById<TextView>(R.id.txt_event_header)
        eventName = bottomSheetDialog.findViewById<EditText>(R.id.edt_event_name)
        callTypeSpinner = bottomSheetDialog.findViewById<Spinner>(R.id.call_type)
        deviceListSpinner = bottomSheetDialog.findViewById<Spinner>(R.id.device_list)
        peopleEdit = bottomSheetDialog.findViewById<EditText>(R.id.add_people)
        startDate = bottomSheetDialog.findViewById<Button>(R.id.start_date)
        startTime = bottomSheetDialog.findViewById<Button>(R.id.start_time)
        endDate = bottomSheetDialog.findViewById<Button>(R.id.end_date)
        endTime = bottomSheetDialog.findViewById<Button>(R.id.end_time)
        description = bottomSheetDialog.findViewById<EditText>(R.id.edt_desc)
        eventDone = bottomSheetDialog.findViewById<ImageButton>(R.id.btn_event_done)
        eventDismiss = bottomSheetDialog.findViewById<ImageButton>(R.id.btn_close)

        // intialize dialog ui data
        var callTypeList = ArrayList<String>()
        callTypeList.add(AppConstants.CALL_TYPE_TEAMS)
        callTypeList.add(AppConstants.CALL_TYPE_ZOOM)

        val adapter: ArrayAdapter<String> =
            ArrayAdapter<String>(requireActivity(), R.layout.row, R.id.text, callTypeList)
        callTypeSpinner?.adapter = adapter
        callTypeSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                callTypeSpinner?.setSelection(position)
                callTypeName = callTypeSpinner?.selectedItem.toString()
            }

        }
        callTypeSpinner?.isEnabled = eventType != AppConstants.EVENT_TYPE_EDIT

        // device spinner ui
        deviceListAdapter =
            ArrayAdapter<String>(requireActivity(), R.layout.row, R.id.text, deviceNamesList)
        deviceListSpinner?.adapter = deviceListAdapter

        deviceListSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                deviceListSpinner?.setSelection(position)
                selectedDeviceItem = assignedDeviceList[position].id
            }

        }
        startDate?.setOnClickListener {
            selectDate(startDate!!)
        }

        startTime?.setOnClickListener {
            selectTime(startTime!!)

        }
        endDate?.setOnClickListener {
            selectDate(endDate!!)
        }

        endTime?.setOnClickListener {
            selectTime(endTime!!)
        }
        eventDismiss?.setOnClickListener {
            dismiss()
        }

        eventDone?.setOnClickListener {

            if (canScheduleMeeting()) {
                val attendencies = peopleEdit?.text.toString()
                val userList = attendencies.split(",")
                val startDateTime = AppUtils.getStartTimeForCreateMeeting(
                    startDate?.text.toString(),
                    startTime?.text.toString()
                )
                val endtDateTime = AppUtils.getStartTimeForCreateMeeting(
                    endDate?.text.toString(),
                    endTime?.text.toString()
                )
                meetingRequestData = MeetingRequestData(
                    "" + eventName?.text.toString(),
                    "" + description?.text.toString(),
                    startDate?.text.toString(),
                    startTime?.text.toString(),
                    endDate?.text.toString(),
                    endTime?.text.toString(),
                    userList,
                    true,
                    startDateTime,
                    AppUtils.getDurationForCreateMeeting(
                        startDateTime,
                        endtDateTime
                    )
                )
                progressBar?.visibility = View.VISIBLE
                Log.d(TAG, "meetingRequestData " + meetingRequestData.toString())
                if (callTypeName?.equals(AppConstants.CALL_TYPE_TEAMS, true) == true) {
                    if (eventType == AppConstants.EVENT_TYPE_EDIT) {
                        editTeamsMeeting(meetingRequestData)
                    } else {
                        TeamsAuthenticationProvider.getToken(
                            requireActivity(),
                            this,
                            TeamsConfig.CREATE_MEETING
                        )
                    }
                } else {
                    if (eventType == AppConstants.EVENT_TYPE_EDIT) {
                        editZoomMeeting(meetingRequestData)
                    } else {
                        ZoomRepository.initializeZoomSDK(context, TeamsConfig.CREATE_MEETING)
                    }
                }
            }
        }
        if (eventType == AppConstants.EVENT_TYPE_EDIT) {
            fillMeetingDetails()
            eventTitle?.text = getText(R.string.str_editEvent)
        }
    }

    private fun fillMeetingDetails() {
        itemMeeting?.let {
            eventName?.setText(itemMeeting?.title)
            if (itemMeeting?.teams?.joinUrl != null) {
                callTypeSpinner?.setSelection(0)
            } else {
                callTypeSpinner?.setSelection(1)
            }
            // TODo there is no participate data in itemMeeting object
            // peopleEdit
            startDate?.text = AppUtils.getDate(itemMeeting?.startTime!!)
            startDate?.setTag(R.id.date_tag, startDate?.text)
            startTime?.text = AppUtils.getDate(itemMeeting?.startTime!!, true)
            startTime?.setTag(R.id.time_tag, startDate?.text)
            endDate?.text = AppUtils.getEndDateWithDurationDate(itemMeeting?.startTime!!,itemMeeting?.duration!!)
            endDate?.setTag(R.id.date_tag, endDate?.text)
            endTime?.text =
                AppUtils.getDate(itemMeeting?.startTime!!, true, itemMeeting?.duration!!)
            endTime?.setTag(R.id.time_tag, endTime?.text)
            description?.setText(itemMeeting?.description)
        }
    }

    private fun editTeamsMeeting(meetingRequestData: MeetingRequestData) {
        LinkletRemoteRepository().updateMeetingAPI(
            itemMeeting?.id!!,
            AppUtils.createMeetingJsonObject(
                true,
                meetingRequestData.meetingSubject,
                meetingRequestData.meetingDescription,
                meetingRequestData.startDateTime,
                meetingRequestData?.timeDuration?.toInt()!!,
                "" + itemMeeting?.teams?.joinUrl,
                arrayListOf(selectedDeviceItem!!)
            )
        )

    }

    private fun editZoomMeeting(meetingRequestData: MeetingRequestData) {
        LinkletRemoteRepository().updateMeetingAPI(
            itemMeeting?.id!!,
            AppUtils.createMeetingJsonObject(
                false,
                meetingRequestData.meetingSubject,
                meetingRequestData.meetingDescription,
                meetingRequestData.startDateTime,
                meetingRequestData.timeDuration.toInt(),
                itemMeeting?.zoom?.joinUrl!!,
                arrayListOf(selectedDeviceItem!!),
                itemMeeting?.zoom?.zoomMeetingId!!, itemMeeting?.zoom?.password

            )
        )
    }

    private fun updateCreateMeetingDevices() {
        deviceListAdapter?.let {
            it.notifyDataSetChanged()
        }
    }

    private fun canScheduleMeeting(): Boolean {
        if (eventName!!.text.isEmpty()) {
            Toast.makeText(requireContext(), "Enter a meeting name", Toast.LENGTH_LONG)
                .show()
            return false
        } else if (peopleEdit!!.text.isEmpty()) {
            Toast.makeText(requireContext(), "Enter participates", Toast.LENGTH_LONG).show()
            return false
        } else if (startDate!!.getTag(R.id.date_tag) == null) {
            Toast.makeText(requireContext(), "Enter start date", Toast.LENGTH_LONG).show()
            return false
        } else if (startTime!!.getTag(R.id.time_tag) == null) {
            Toast.makeText(requireContext(), "Enter start time", Toast.LENGTH_LONG).show()
            return false
        } else if (endDate!!.getTag(R.id.date_tag) == null) {
            Toast.makeText(requireContext(), "Enter end date", Toast.LENGTH_LONG).show()
            return false
        } else if (endTime!!.getTag(R.id.time_tag) == null) {
            Toast.makeText(requireContext(), "Enter end time", Toast.LENGTH_LONG).show()
            return false
        } else if (description!!.text.isEmpty()) {
            Toast.makeText(requireContext(), "Enter description", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun selectTime(timeView: Button) {
        val timePicker: TimePickerDialog = TimePickerDialog(
            requireContext(),
            { view, hourOfDay, minute ->
                timeView.text = String.format("%02d:%02d:%02d", hourOfDay, minute, 0)
                timeView.setTag(R.id.time_tag, timeView.text)
                timeView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
            },
            12,
            10,
            true
        )
        timePicker.show()

    }

    private fun selectDate(dateView: Button) {
        val c: Calendar = Calendar.getInstance()
        val mYear: Int = c.get(Calendar.YEAR) // current year
        val mMonth: Int = c.get(Calendar.MONTH) // current month
        val mDay: Int = c.get(Calendar.DAY_OF_MONTH) // current day

        val datePickerDialog = DatePickerDialog(
            requireActivity(),
            { view, year, monthOfYear, dayOfMonth -> // set day of month , month and year value in the edit text
                val calendar = Calendar.getInstance()
                calendar[year, monthOfYear] = dayOfMonth
                val format = SimpleDateFormat("yyyy-MM-dd")
                val strDate: String = format.format(calendar.time)
                dateView.text = "" + strDate
                dateView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                dateView.setTag(R.id.date_tag, dateView.text)

            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }

    override fun onUserAuthenticationSucess(
        graphAPIToken: String,
        communicationAPItoken: String, requestType: Int
    ) {
        when (requestType) {
            TeamsConfig.CREATE_MEETING ->
                createTeamsMeeting(graphAPIToken, meetingRequestData, selectedDeviceItem!!)
        }
    }

    override fun onAuthenticationFailed(error: String, requestType: Int) {
        Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
        dismiss()
    }


    // Teams create meeting
    private fun createTeamsMeeting(
        graphToken: String,
        meetingRequestData: MeetingRequestData,
        selectedDeviceItem: String
    ) {
        runBlocking {
            val meetingURL: String? =
                withContext(Dispatchers.IO) {
                    TeamsRepository.createMeeting(graphToken, meetingRequestData)
                }
            Toast.makeText(
                requireContext(),
                "Meeting created successfully:$meetingURL",
                Toast.LENGTH_LONG
            ).show()

            meetingURL?.let {
                LinkletRemoteRepository().createMeetingAPI(
                    AppUtils.createMeetingJsonObject(
                        true,
                        meetingRequestData.meetingSubject,
                        meetingRequestData.meetingDescription,
                        meetingRequestData.startDateTime,
                        meetingRequestData.timeDuration.toInt(),
                        meetingURL!!,
                        arrayListOf(selectedDeviceItem!!)
                    )
                )
                //  dismiss progress handle in create meeting api reposnse observer
            } ?: kotlin.run {
                Toast.makeText(
                    requireContext(),
                    "Creating Microsoft teams meeting failed",
                    Toast.LENGTH_LONG
                ).show()
                dismiss()
            }
        }
    }

    // Zoom functionality creating meeting once we get the zoom token, will be called from
// parent activity
    fun getZoomToken(zoomToken: String) {
        Log.d("Zoom token---", zoomToken)
        Log.d(TAG,"meetingRequestData after browser coming back "+meetingRequestData)
        ZoomRepository.parseCode(zoomToken, meetingRequestData,selectedDeviceItem)
    }

}