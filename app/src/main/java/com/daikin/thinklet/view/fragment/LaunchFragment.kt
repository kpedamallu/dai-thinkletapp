package com.daikin.thinklet.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.callback.Callback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.daikin.thinklet.R
import com.daikin.thinklet.model.PreferencesManager
import com.daikin.thinklet.view.HomeScreenActivity


class LaunchFragment : BaseFragment() {
    private lateinit var sign_up_button : Button
    lateinit var sign_in_button : Button
    private lateinit var account: Auth0
    private var cachedCredentials: Credentials? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_launch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        account = Auth0(
            getString(R.string.com_auth0_client_id),
            getString(R.string.com_auth0_domain)
        )

        sign_up_button = requireView().findViewById<View>(R.id.sign_up_button) as Button
        sign_in_button = requireView().findViewById<View>(R.id.sign_in_button) as Button
        sign_up_button.setOnClickListener {
            loginWithBrowser()
        }
        sign_in_button.setOnClickListener {
            loginWithBrowser()
        }
    }

    private fun loginWithBrowser() {
        // Setup the WebAuthProvider, using the custom scheme and scope.
        WebAuthProvider.login(account)
            .withScheme(getString(R.string.com_auth0_scheme))
            .withScope("openid profile email")
            .withAudience(getString(R.string.com_auth0_audience))

            // Launch the authentication passing the callback where the results will be received
            .start(requireContext(), object : Callback<Credentials, AuthenticationException> {
                override fun onFailure(exception: AuthenticationException) {
                    Toast.makeText(activity, "Failure: ${exception.getCode()}", Toast.LENGTH_LONG).show()
                }

                override fun onSuccess(credentials: Credentials) {
                    cachedCredentials = credentials
                    println("MainActivity.onSuccess token = " + credentials.accessToken)
                    println("MainActivity.onSuccess expiresAt = " + credentials.expiresAt)

                    //saving user access token
                    PreferencesManager.put(credentials.accessToken, PreferencesManager.KEY_USER_AUTH_TOKEN)

                    val intent = Intent(context, HomeScreenActivity::class.java)
                    startActivity(intent)
                }
            })
    }
}