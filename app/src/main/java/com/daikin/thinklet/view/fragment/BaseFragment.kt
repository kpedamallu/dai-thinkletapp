package com.daikin.thinklet.view.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.daikin.thinklet.R
import com.daikin.thinklet.model.MeetingRequestData
import com.daikin.thinklet.repository.external.TeamsRepository
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository
import com.daikin.thinklet.utils.AppUtils
import com.microsoft.graph.models.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext


open class BaseFragment : Fragment() {
    private val TAG = "BaseFragment"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun joinMeeting(communicationToken: String, url: String, graphToken: String) {
        runBlocking {
            val user: User? =
                withContext(Dispatchers.IO) {
                    TeamsRepository.getUserProfile(
                        graphToken
                    )
                }
            Log.d(TAG, "received user info " + user?.displayName)
            user?.let {
                TeamsRepository.startCallComposite(
                    communicationToken,
                    url,
                    requireContext(),
                    "" + user.displayName,
                    "" + user.id
                )
            } ?: kotlin.run {
                Toast.makeText(
                    requireContext(),
                    "Fetching teams user object is null",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }
}