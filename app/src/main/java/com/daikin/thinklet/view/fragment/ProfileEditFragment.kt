package com.daikin.thinklet.view.fragment

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.daikin.thinklet.R
import com.daikin.thinklet.model.PreferencesManager
import com.daikin.thinklet.model.UserProfile
import com.daikin.thinklet.repository.remote.LinkletRemoteRepository
import com.google.android.material.imageview.ShapeableImageView
import java.io.IOException


class ProfileEditFragment : BaseFragment() {

    private lateinit var profileEditPicture: ShapeableImageView
    private lateinit var backArrow: ImageView
    private lateinit var updateButton: Button
    private val REQUEST_CAMERA_PERMISSION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_profile_edit, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cameraLayout : ConstraintLayout = view.findViewById(R.id.profile_edit_bg)

        backArrow = view.findViewById(R.id.back_icon)

        backArrow.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStackImmediate()
        }
        PreferencesManager?.get<UserProfile>(PreferencesManager.KEY_PROFILE).let {
            println("ProfileFragment.onViewCreated Observe $it")

            var emailEditText = requireView().findViewById<View>(R.id.email_edit_text) as EditText
            emailEditText.setText(it?.email)
        }

        profileEditPicture= view.findViewById(R.id.profile_edit_picture)
        updateButton = view.findViewById(R.id.update_btn)

        updateButton.setOnClickListener {
            Toast.makeText(context,"Coming soon",Toast.LENGTH_SHORT).show()
        }

        cameraLayout.setOnClickListener {

            if (checkPermission()) {
                openAlertDialog()
            }else{
                requestPermission();
            }
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            requireContext() as Activity, arrayOf(android.Manifest.permission.CAMERA),
            REQUEST_CAMERA_PERMISSION
        )
    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CAMERA_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openAlertDialog()
            } else {
                Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED
                    ) {
                        showMessageOKCancel("You need to allow access permissions",
                            DialogInterface.OnClickListener { dialog, which ->
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermission()
                                }
                            })
                    }
                }
            }
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    private fun openAlertDialog(){
        val dialogView = LayoutInflater.from(context).inflate(R.layout.camera_dialog, null)

        val alertDialog = AlertDialog.Builder(context)
            .setView(dialogView)
            .setCancelable(true)
            .create()

        val mOpenCamera = dialogView.findViewById<TextView>(R.id.open_camera)
        mOpenCamera.setOnClickListener {

            openCamera()

            alertDialog.dismiss()
        }

        // Set a click listener on the front camera button
        val chooseGallery = dialogView.findViewById<TextView>(R.id.choose_gallery)
        chooseGallery.setOnClickListener {

            chooseImageFromGallery()

            alertDialog.dismiss()
        }

        alertDialog.show()
    }
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_CODE_IMAGE_PICK = 2

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    private fun chooseImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_CODE_IMAGE_PICK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var bitmap: Bitmap? = null
        val desiredWidth = 144
        val desiredHeight = 144
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            bitmap = data!!.extras!!.get("data") as Bitmap?
            bitmap = Bitmap.createScaledBitmap(bitmap!!, desiredWidth, desiredHeight, false)
            profileEditPicture.setImageBitmap(bitmap)
        }
        if (requestCode == REQUEST_CODE_IMAGE_PICK && resultCode == RESULT_OK){
            val selectedImageUri: Uri? = data!!.data
//            profileEditPicture.setImageURI(selectedImageUri)
            try {
                val inputStream = context?.contentResolver?.openInputStream(selectedImageUri!!)
                bitmap = BitmapFactory.decodeStream(inputStream)
                inputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            bitmap = Bitmap.createScaledBitmap(bitmap!!, desiredWidth, desiredHeight, false)
            profileEditPicture.setImageBitmap(bitmap)
        }
    }

}