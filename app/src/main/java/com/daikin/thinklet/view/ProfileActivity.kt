package com.daikin.thinklet.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.daikin.thinklet.R
import com.daikin.thinklet.view.fragment.HomeScreenFragment
import com.daikin.thinklet.view.fragment.ProfileFragment

class ProfileActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        replaceFragment(ProfileFragment())
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.profile_container, fragment).addToBackStack(null)
            .commit();
    }
}