package com.daikin.thinklet.database.dbmodels

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.daikin.thinklet.model.UserData

@Entity(tableName = "tb_user")
class TBUSer(
    @PrimaryKey
    val userId: Long,
    val userName: String
) {
    companion object {
        fun from(user: UserData): TBUSer {
            return TBUSer(
                userId = user.userId,
                userName = user.userName
            )
        }
    }

    fun toUser(): UserData {
        return UserData( userId, userName)
    }
}