package com.daikin.thinklet.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.daikin.thinklet.database.dbmodels.TBUSer

@Dao
public interface UserDao {

    @Query("SELECT * FROM tb_user where userId== :userID")
    fun getUser(userID: Long): LiveData<TBUSer>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: TBUSer)
}
