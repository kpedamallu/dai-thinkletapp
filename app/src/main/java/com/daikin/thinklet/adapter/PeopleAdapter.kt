package com.daikin.thinklet.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.daikin.thinklet.R
import java.lang.IndexOutOfBoundsException

class PeopleAdapter(context: Activity, resourceId: Int, list: List<String>) :
    ArrayAdapter<String>(context, resourceId, list) {

    private var inflater: LayoutInflater = context.layoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var view = convertView
        if (view == null)
            view = inflater.inflate(R.layout.people_spinner, null) as LinearLayout

        return createItemView(position, view!!, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null)
            view = inflater.inflate(R.layout.people_spinner, null) as LinearLayout

        return createItemView(position, view!!, parent)
    }

    private fun createItemView(position: Int, convertView: View, parent: ViewGroup): View {

        val rowItem = getItem(position)
        val txtTitle = convertView!!.findViewById<View>(R.id.text) as TextView
        txtTitle.text = rowItem!!
        return convertView
    }
}
