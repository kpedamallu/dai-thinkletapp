package com.daikin.thinklet.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.daikin.thinklet.R
import com.daikin.thinklet.model.ItemsMeeting
import com.daikin.thinklet.model.Zoom
import com.daikin.thinklet.utils.AppUtils


interface OnMeetingClickListener {
    fun onItemClick(itemsMeeting: ItemsMeeting)
    fun onJoinMeetingClick(meetingURL: String)
    fun onZoomMeetingClick(meetingURL: Zoom)
}

class CalenderAdapter(
    private val calenderDataList: ArrayList<ItemsMeeting>,
    context: Context?,
    private var listener: OnMeetingClickListener
) : RecyclerView.Adapter<CalenderAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val context = itemView.context
        fun bind(calenderData: ItemsMeeting) {
            val name: TextView = itemView.findViewById(R.id.meeting_name)
            val joinNow: TextView = itemView.findViewById(R.id.join_now_text)
            val time: TextView = itemView.findViewById(R.id.time)
            val dateLayout: ConstraintLayout = itemView.findViewById(R.id.day_date_layout)
            val meetingLayout: ConstraintLayout = itemView.findViewById(R.id.meeting_layout)
            name.text = calenderData.title
            time.text = AppUtils.convertDate(calenderData.startTime!!, true) + " - " +
                    AppUtils.convertDate(calenderData.startTime!!, true, calenderData.duration!!)


            val dateText: TextView = itemView.findViewById(R.id.day_text_view)
            dateText.text = AppUtils.convertDate(calenderData.startTime!!).split(' ').toTypedArray()[0]

            val date_text_view: TextView = itemView.findViewById(R.id.date_text_view)
            date_text_view.text = AppUtils.convertDate(calenderData.startTime!!).split(' ').toTypedArray()[1]

//            if (calenderData.isSameDate) {
//                dateLayout.visibility = View.VISIBLE
//            } else {
//                dateLayout.visibility = View.INVISIBLE
//            }
//            if (calenderData.teams?.joinUrl == null) {
//                joinNow.visibility = View.VISIBLE
//                meetingLayout.background = context.getDrawable(R.drawable.with_join_now_bg)
//            } else {
//                joinNow.visibility = View.INVISIBLE
//                meetingLayout.background = context.getDrawable(R.drawable.without_join_now)
//            }

            if (calenderData.teams?.joinUrl == null) {
                // Zoom
                val teams_icon_layout: ConstraintLayout = itemView.findViewById(R.id.teams_icon_layout)
                teams_icon_layout.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.zoom_bg))

                val icon: ImageView = itemView.findViewById(R.id.teams_icon)
                icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.zoom))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.calender_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(calenderDataList[position])
        holder.itemView.setOnClickListener {
            listener.onItemClick(calenderDataList[position])
        }
        holder.itemView.findViewById<TextView>(R.id.join_now_text).setOnClickListener {
            //TODO pass user selected meeting link to this method to join into the call
            if (calenderDataList[position].teams?.joinUrl == null) {
                calenderDataList[position].zoom?.let { it1 -> listener.onZoomMeetingClick(it1) }
            } else{
//                listener.onJoinMeetingClick("https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTQ0OWQyMGItNGYyMy00MTM4LWExZGItYmEwMjAyMWMyYjg2%40thread.v2/0?context=%7b%22Tid%22%3a%2246439914-63b7-464e-8327-9b13737ae8cf%22%2c%22Oid%22%3a%222a0294a2-abfb-4f81-8e86-6851e7c54053%22%7d")
                listener.onJoinMeetingClick(calenderDataList[position].teams?.joinUrl!!)
            }
        }
    }

    override fun getItemCount(): Int {
        return calenderDataList.size
    }
}
